import initialState from './../initialState';
import * as types from './types';

export default function userReducer(state = initialState.user, action) {
  switch (action.type) {
    case types.USER_LOGIN_SUCCESS:
      return action.user
    default:
      return state
  }
}