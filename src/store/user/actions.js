import * as types from './types';

export function userLoginSuccess(user) {
  return { type: types.USER_LOGIN_SUCCESS, user}
}