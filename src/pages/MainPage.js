import React, { Component } from 'react';
import { Container } from '../styles/styled-container';
import styled, { css } from 'styled-components';

import Footer from './default/Footer';
// import { Button } from '../styles/styled-button';
import { mediaUp, mediaDown } from '../styles/styled-breakpoints';
import { Header } from '../styles/styled-header';

import logo from '../assets/images/party.png';

const SectionsWrapper = styled.div`
    display: flex;
    flex-direction: column;

    ${ mediaUp.desktop`
        max-width: 80%;
        flex-direction: row;
        margin: 0 auto;
    `}

    ${props => props.textLeft && css`
        text-align: center;

        ${ mediaUp.desktop`
            text-align: left;
        `}

        ${ mediaDown.desktop`
            section {
                &:first-of-type {
                    order: 2;
                }

                &:last-of-type {
                    order: 1;
                }
            }
        `}
    `}

    ${props => props.textRight && css`
        text-align: center;

        ${ mediaUp.desktop`
            text-align: right;
        `}
    `}
`;

const Section = styled.section`
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: 20px 0;
    padding: 0 15px;

    ${ mediaUp.desktop`
        width: 50%;
        margin: 50px 0;
    `}

    h2 {
        color: #333;
    }

    p {
        color: #555;
    }

    img {
        width: 100%;
        height: auto;
    }
`;

class MainPage extends Component {
    render() {
        return (
        <div>
            <Container>
                <Header>TokenEvent działa tak jak tego potrzebujesz!</Header>
                <SectionsWrapper textRight>
                    <Section >
                        <h2>Kup żetony gdzie chcesz!</h2>
                        <p>Z naszym serwisem zakupisz swoje żetony jak i gdzie tylko chcesz. 
                        Możesz zrobić to w domu przed rozpoczęciem się imprezy, dokładnie zaplanować co chcesz kupić wczesniej, lub spontanicznie w czasie jej trwania. Bez kolejek, bez gotówki i bez zbędnych kartonowych wymienników.
                        Prosto i szybko, wszystko w twoim telefonie! </p>
                    </Section>
                    <Section>
                        <img src={logo} alt='' />
                    </Section>
                </SectionsWrapper>

                {/* <SectionsWrapper textLeft>
                    <Section>
                        <img src={logo} alt='' />
                    </Section>
                    <Section>
                        <h2>Kup żetony gdzie chcesz!</h2>
                        <p>Z naszym serwisem zakupisz swoje żetony jak i gdzie tylko chcesz. 
                        Mozesz zrobić to w domu przed rozpoczęciem się imprezy, dokladnie zaplanować co chcesz kupić wczesniej, lub spontanicznie w czasie jej trwania. Bez kolejek, bez gotówki i bez zbednych kartonowych wymienników.
                        Prosto i szybko wszystko w twoim telefonie! </p>
                        <Button primary>Czytaj więcej</Button> 
                    </Section>
                </SectionsWrapper> */}

                </Container>
            <Footer />
        </div>
        );
    }
}

export default MainPage;
