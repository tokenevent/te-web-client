import React, {Component} from 'react';
import {Form} from 'informed';
import Swal from 'sweetalert2';

import userService from '../services/UserService';
import logo from '../assets/images/logoWhiteMedium.png';
import {app} from '../utils/enums';
import {USERNAME_TAKEN, BAD_PROMO_CODE} from '../utils/messages'
import {isEmail, isPassword} from '../utils/validators';
import {ErrorText} from '../components/FormComponents/ErrorText';
import LoadingSpinner from '../components/LoadingSpinner';
import {isRequired} from './../utils/validators';

import {
    ColorContainer,
    LogoWrapper,
    RegisterFormWrapper,
    FormInputWrapper,
    RedirectLinkWrapper,
    SubmitButton,
    SubmitErrorMessage,
    DoubleInputWrapper,
    DoubleFormInputWrapper
} from '../styles/styled-login-register';

class RegisterPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            userTaken: false
        };
        this.processRegister = this.processRegister.bind(this);
        this.setFormApi = this.setFormApi.bind(this);
    }

    setFormApi = formApi => this.formApi = formApi;

    equalsPassword = (text, errorMessage) => {
        return this.formApi.getValue('password') !== text ? errorMessage : undefined;
    };

    processRegister = values => {
        delete values.retypedPassword;
        this.setState({ isLoading: true }, () => {
            userService.createUser(values)
                .then(response => this.registerUserSuccessHandler(response))
                .catch(error => this.registerUserErrorHandler(error));
        });
    };

    registerUserSuccessHandler = response => {
        Swal('Utworzone!', 'Konto zostało utworzone, możesz się zalogować.', 'success')
        this.setState({userTaken: false, isLoading: false});
        this.props.history.push(app.LOGIN_PAGE);
    };

    registerUserErrorHandler = error => {
        if(error.response.data !== null) {
            if (error.response.data.message === USERNAME_TAKEN) {
                this.setState({userTaken: true, isLoading: false})
            }
            else if (error.response.data.message === BAD_PROMO_CODE) {
                this.setState({isLoading: false})
                Swal('Oops...', 'Wprowadziles niepoprawny kod promocyjny', 'error')
            } else {
                Swal('Oops...', 'Coś poszło nie tak!', 'error')
            }
        } else {
            this.setState({ isLoading: false})
            Swal('Oops...', 'Coś poszło nie tak!', 'error')
        }
    };

    render() {
        const { isLoading } = this.state;
        return (
            <ColorContainer>
                <LogoWrapper>
                    <a href={app.HOMEPAGE}>
                        <img src={logo} alt="Tokenevent Logo" />
                    </a>
                </LogoWrapper>
                <RegisterFormWrapper>
                    <h1>Zarejestruj się</h1>
                    {isLoading && <LoadingSpinner />}
                    <Form onSubmit={this.processRegister} getApi={this.setFormApi} field="registerForm" id="registerForm">
                        <FormInputWrapper>
                            <label htmlFor="registerFirstName">Nazwa użytkownika</label>
                            <ErrorText
                                field="username"
                                validate={value => isRequired(value, "* Wymagane pole")}
                                validateonchange="true"
                                validateonblur="true"
                                id="registerUsername"
                                placeholder="Nazwa użytkownika"
                            />
                        </FormInputWrapper>

                        <DoubleInputWrapper>
                            <DoubleFormInputWrapper>
                                <label htmlFor="registerFirstName">Imię</label>
                                <ErrorText
                                    field="firstName"
                                    validate={value => isRequired(value, "* Wymagane pole")}
                                    validateonchange="true"
                                    validateonblur="true"
                                    id="registerFirstName"
                                    placeholder="Imię"
                                />
                            </DoubleFormInputWrapper>

                            <DoubleFormInputWrapper>
                                <label htmlFor="registerLastName">Nazwisko</label>
                                <ErrorText
                                    field="lastName"
                                    validate={value => isRequired(value, "* Wymagane pole")}
                                    validateonchange="true"
                                    validateonblur="true"
                                    id="registerLastName"
                                    placeholder="Nazwisko"
                                />
                            </DoubleFormInputWrapper>
                        </DoubleInputWrapper>

                        <DoubleInputWrapper>
                            <DoubleFormInputWrapper>
                                <label htmlFor="registerEmail">E-mail</label>
                                <ErrorText
                                    field="email"
                                    validate={value => isEmail(value, "Podaj prawidłowy adres e-mail")}
                                    validateonchange="true"
                                    validateonblur="true"
                                    id="registerEmail"
                                    placeholder="E-mail"
                                />
                            </DoubleFormInputWrapper>

                            <DoubleFormInputWrapper>
                                <label htmlFor="registerPhone">Telefon</label>
                                <ErrorText
                                    field="phone"
                                    type="tel"
                                    validate={value => isRequired(value, "* Wymagane pole")}
                                    validateonchange="true"
                                    validateonblur="true"
                                    id="registerPhone"
                                    placeholder="Telefon"
                                />
                            </DoubleFormInputWrapper>
                        </DoubleInputWrapper>

                        <FormInputWrapper>
                            <label htmlFor="registerPassword">Hasło</label>
                            <ErrorText
                                field="password"
                                type="password"
                                validate={value => isPassword(value, "Hasło musi mieć min. 8 znaków, 1 cyfrę, 1 dużą i 1 małą literę")}
                                validateonchange="true"
                                validateonblur="true"
                                id="registerPassword"
                                placeholder="Hasło"
                            />
                        </FormInputWrapper>

                        <FormInputWrapper>
                            <label htmlFor="repeatRegisterPassword">Powtórz hasło</label>
                            <ErrorText
                                field="retypedPassword"
                                type="password"
                                validate={value => this.equalsPassword(value, "Hasła muszą do siebie pasować")}
                                validateonchange="true"
                                validateonblur="true"
                                id="repeatRegisterPassword"
                                placeholder="Powtórz hasło"
                            />
                        </FormInputWrapper>

                        <FormInputWrapper>
                            <label htmlFor="promoCode">Kod promocyjny</label>
                            <ErrorText
                                field="promoCode"
                                // validate={value => isRequired(value, "* Wymagane pole")}
                                validateonchange="true"
                                validateonblur="true"
                                id="promoCode"
                                placeholder="Kod promocyjny"
                            />
                        </FormInputWrapper>

                        <SubmitButton type="submit">Zarejestruj</SubmitButton>
                        {this.state.userTaken ?
                            <SubmitErrorMessage>Nazwa użytkownika jest już zajęta</SubmitErrorMessage> : ''}
                    </Form>
                    <RedirectLinkWrapper>
                        Masz już konto? <a href={app.LOGIN_PAGE}>Zaloguj się!</a>
                    </RedirectLinkWrapper>
                </RegisterFormWrapper>
            </ColorContainer>
        );
    }
}

export default RegisterPage;
