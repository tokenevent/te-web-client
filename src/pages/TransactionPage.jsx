import React, {Component} from 'react';
import { Container } from '../styles/styled-container';
import { Header } from '../styles/styled-header';
import Footer from './default/Footer';
import { Redirect } from 'react-router-dom';
import tokenService from '../services/TokenService';
import styled from 'styled-components';
import {groupBy, countPLN, checkLogedIn} from '../utils/helpers';
import {
  TokensWrapper,
  SingleTokenWrapper
} from '../styles/styled-token-kinds';
import { AcceptGreen, RejectRed } from '../styles/colors';
import TokenKind from "../components/TokenKind/TokenKind";

const SumAllWrapper = styled.div`
  font-weight: bold;
  text-align: center;
  margin-top: 10px;
  margin-bottom: 20px;
  font-size: 31px;
`;

const ButtonsWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const AcceptButton = styled.button`
    width: 100%;
    max-width: 240px;
    border: 1px solid ${AcceptGreen};
    background-color: ${AcceptGreen};
    color: white;
    border-radius: 0;
    margin-bottom: 20px;
    height: 40px;
    transition: all .3s ease-in-out;

    &:hover {
      background-color: white;
      color: ${AcceptGreen};
    }
`;

const RejectButton = styled(AcceptButton)`
    border: 1px solid ${RejectRed};
    background-color: ${RejectRed};

    &:hover {
      background-color: white;
      color: ${RejectRed};
    }
`;

class TransactionPage extends Component {
    constructor(props) {
      super(props);
      this.state = {
          transactionId: null,
          redirectBack: false,
          redirectAccept: false,
          redirectReject: false,
          tokens: []
      }
    }

    componentDidMount = () => {
      if(this.props.location.state !== undefined) {
        this.setState({
          redirectBack: false,
          transactionId: this.props.location.state.transId,
          tokens: groupBy(this.props.location.state.trans.tokens, 'buyableTokenName')
        })
      } else {
        this.setState({
          redirectBack: true
        })
      }
    };

    TransactionHandle = (decision) => {
      const payload = {
        transactionId: this.state.transactionId,
        decision: decision
      }
      tokenService.postTransactionStatus(payload)
            .then(res => this.getTransSuccessHandler(res, decision))
            .catch(error => this.getTransErrorHandler(error))
    };

    getTransSuccessHandler = (res, decision) => {
      (decision === "ACCEPT") ? this.setState({ redirectAccept: true }) : this.setState({ redirectReject: true });
    }

    getTransErrorHandler = error => {
      checkLogedIn(error);
      this.setState({ redirectReject: true });
    }

    render() {
        const {redirectBack, redirectAccept, redirectReject, tokens} = this.state;
        let sum = 0;

        Object.keys(this.state.tokens).map(token => {
          sum += this.state.tokens[token][0].tokenValue * this.state.tokens[token].length;
          return '';
        });

        return (
            <div>
              { redirectBack && <Redirect to={'/wallet'}/>}
              { redirectAccept && <Redirect to={'/accepted'}/>}
              { redirectReject && <Redirect to={'/rejected'}/>}
                <Container>
                    <Header>Weryfikacja transakcji</Header>
                    <TokensWrapper>
                      {Object.keys(tokens).map((tokenName) => {
                        return(<SingleTokenWrapper key={tokens[tokenName][0].id}>
                          <TokenKind
                            name={tokenName}
                            unitPrice={countPLN(tokens[tokenName][0].tokenValue)}
                            quantity={tokens[tokenName].length}
                            totalAmount={countPLN(tokens[tokenName].length * tokens[tokenName][0].tokenValue)}
                          />
                        </SingleTokenWrapper>)
                      })}
                    </TokensWrapper>
                    <SumAllWrapper>W sumie: {countPLN(sum)}</SumAllWrapper>
                    <ButtonsWrapper>
                      <AcceptButton onClick={ () => this.TransactionHandle('ACCEPT')}>Akceptuj</AcceptButton>
                      <RejectButton onClick={ () => this.TransactionHandle('REJECT')}>Odrzuć</RejectButton>
                    </ButtonsWrapper>
                </Container>
                <Footer />
            </div>
        );
    }
}

export default TransactionPage;
