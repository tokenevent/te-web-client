import React, {Component} from 'react';
import Footer from './default/Footer';
import { Container } from '../styles/styled-container';
import BuyTokenComponent from '../components/BuyTokenComponent';
import {app} from './../utils/enums';
import LoadingSpinner from '../components/LoadingSpinner';

class BuyTokenPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            event: []
        };
    }

    componentDidMount = () => {
        if (!this.props.location.event) {
            this.props.history.push(app.HOMEPAGE);
        }
        this.setState({ event: this.props.location.event, isLoading: false });
     };

    render() {
        const { isLoading, event } = this.state;
        return (
            <div>
                <Container>
                    { isLoading && <LoadingSpinner />}
                    { (!event.length && !isLoading) && 
                    <BuyTokenComponent
                        event={event}
                    />}
                </Container>
                <Footer />
            </div>
        );
    }
}

export default BuyTokenPage;
