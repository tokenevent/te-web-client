import React, { useState, useEffect} from 'react';
import { Header } from "../../../styles/styled-header";
import AdminWrapperComponent from "../../../components/AdminWrapperComponent";
import EventService from '../../../services/EventService';
import LoadingSpinner from '../../../components/LoadingSpinner';
import styled from 'styled-components';
import {formatDate} from '../../../utils/helpers';
import EventMap from '../../../components/event/EventMap';
import {LinkButton, DeleteButton} from '../../../styles/styled-button';

const Description = styled.p`
  font-size: 14px;
  color: #555;
`;

const DateWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 15px;
`;

const AddressWrapper = styled(DateWrapper)``;
const ButtonsWrapper = styled(DateWrapper)`
  margin-top: 15px;
`;

const EventMoreInformationPage = ({ match }) => {
  const eventId = match.params.eventId;
  const [event, setEvent] = useState(null);

  useEffect(() => {
    EventService.getEvent(eventId)
      .then(res => setEvent(res.data))
      .catch(err => console.log(err))
  }, [eventId]);

  return <AdminWrapperComponent>
    { !event && <LoadingSpinner />}
    { event && <>
      <Header>Więcej informacji o {event.name}</Header>
      <DateWrapper>
        <div>Start: {formatDate(event.startingAt)}</div>
        <div>Koniec: {formatDate(event.endingAt)}</div>
      </DateWrapper>
      <AddressWrapper>
        <div>Miasto: {event.address.city}, {event.address.zipCode}</div>
        <div>Ulica: {event.address.street}</div>
      </AddressWrapper>
      <Description>{event.description}</Description>
      <EventMap
          lat={event.address.coordinates.lat}
          lon={event.address.coordinates.lon}
          eventName={event.name}
      />
      <ButtonsWrapper>
        <DeleteButton>Usuń wydarzenie</DeleteButton>
        <LinkButton href="#">Edytuj Wydarzenie</LinkButton>
      </ButtonsWrapper>
    </>}
  </AdminWrapperComponent>
};

export default EventMoreInformationPage;
