import React, { useState } from "react";
import { ScannerFormWrapper } from "./ManageScannersPage.style";
import AddScannerForm from "./AddScannerForm";
import EventManagerService from "../../../services/EventManagerService";
import Swal from 'sweetalert2';

const ScannerForm = ({ eventId, fetchScanners }) => {
  const [formApi, setFormApi] = useState(null);
  console.log(formApi);

  const onSubmit = values => {
    EventManagerService.addScanner(eventId, values)
      .then(res => {
        console.log(res);
        Swal('Sukces!', 'Skaner został dodany', 'success');
        fetchScanners(eventId);
      })
      .catch(er => {
        console.log('err', er);
        Swal('Błąd', 'Wystąpił błąd.', 'error');
      })
  };

  const getFormApi = formApi => setFormApi(formApi);

  return <ScannerFormWrapper>

    <AddScannerForm
      onSubmit={onSubmit}
      getFormApi={getFormApi}
    />
  </ScannerFormWrapper>;
};

export default ScannerForm
