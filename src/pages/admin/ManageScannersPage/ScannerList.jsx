import React from 'react'
import { IconsWrapper, ScannerRow, Wrapper } from './ScannerList.style';
import { LinkButton, DeleteButton } from "../../../styles/styled-button";
import { app } from "../../../utils/enums";


const ScannersList = ({ scanners, deleteScanner }) => {
  return <Wrapper>
    {scanners.map((s, i) => {
      return <ScannerRow key={i}>
        <div>{s.username}</div>
        <IconsWrapper>
          <LinkButton href={`${app.ADMIN_PANEL}/scanner-history/${s.id}`}>Transakcje </LinkButton>
          <DeleteButton onClick={() => deleteScanner(s.id)}>x</DeleteButton>
        </IconsWrapper>
      </ScannerRow>
    })}
  </Wrapper>
};
export default ScannersList;
