import React from 'react';
import { formatDate } from "../../../utils/helpers";
import StatusCell from "../../../components/StatusCell/StatusCell";
import TransactionTable from "../../../components/TransactionTable/TransactionTable";

const HistoryList = ({ historyList }) => {

  const columns = React.useMemo(
    () => [
      {
        Header: '',
        accessor: 'id',
        hideInMobile: true,
        Cell: ({ row }) => row.index + 1
      },
      {
        Header: 'Status',
        accessor: 'status',
        hideLabelInMobile: true,
        Cell: ({ row }) => <StatusCell status={row.original.status}/>,
      },
      {
        Header: 'Wydarzenie',
        accessor: 'eventName'
      },
      {
        Header: 'Data',
        accessor: 'statusChangedAt',
        Cell: ({ row }) => formatDate(row.original.statusChangedAt)
      },
      {
        Header: 'Użytkownik',
        accessor: 'userName',
      },

      {
        Header: 'Ilość',
        accessor: 'tokens.count'
      },
      {
        Header: 'Wartość',
        accessor: 'tokens.totalValue',
        Cell: ({ row }) => `${row.original.tokens.totalValue / 100}zł`
      },
    ],
    []
  );

  return <>
    <TransactionTable
      columns={columns}
      data={historyList}
    />
  </>
};

export default HistoryList;
