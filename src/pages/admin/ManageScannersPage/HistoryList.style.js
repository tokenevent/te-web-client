import styled from "styled-components";
import { AcceptGreen, PendingOrange, RejectRed } from "../../../styles/colors";

export const StatusColor = styled.span`
    color:  ${({ color }) =>
  color === 'ACCEPTED' && AcceptGreen || // eslint-disable-line
  color === 'REJECTED' && RejectRed || // eslint-disable-line
  color === 'PENDING' && PendingOrange || // eslint-disable-line
  color === 'COMPLETED' && AcceptGreen || // eslint-disable-line
  color === 'CANCELED' && RejectRed // eslint-disable-line
  };
`;
