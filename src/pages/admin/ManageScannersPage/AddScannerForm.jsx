import React from 'react';
import { Form } from 'informed';
import { isRequired } from './../../../utils/validators';
import styled from 'styled-components';
import { primary } from '../../../styles/colors';
import { Button } from '../../../styles/styled-button';
import { ErrorText } from "../../../components/FormComponents/ErrorText";

const FormInputWrapper = styled.div`
    display: flex;
    flex-direction: column;

    input {
        display: block;
        border: 1px solid ${primary};
        color: black;
        padding: 5px;
        width: 100%;
        padding: 10px;
        line-height: 1.4;

        &#date {
            width: 100%;
        }
    }

    .react-datepicker__input-container {
        width: 100%;
    }
`;

const WrapDoubles = styled.div`
    display: flex;
    justify-content: space-between;
`;

const DoubleFormInputWrapper = styled(FormInputWrapper)`
    width: 49%;
`;

const FormInputLabel = styled.label`
    display: block;
    color: ${primary};
    font-size: 12px;
    margin: 8px 0;
`;

const ButtonWrapper = styled.div`
    margin-top: 20px;
`;

class AddScannerForm extends React.Component {
  render() {
    const { onChange, onSubmit, getFormApi } = this.props;

    return (
      <Form onChange={onChange} onSubmit={onSubmit} getApi={getFormApi}>
        <WrapDoubles>
          <DoubleFormInputWrapper>
            <FormInputLabel htmlFor="username">Nazwa użytkownika</FormInputLabel>
            <ErrorText
              field="username"
              validate={value => isRequired(value, "Wymagane")}
              validateonchange="true"
              validateonblur="true"
              id="username"
              placeholder="Nazwa użytkownika"
            />
          </DoubleFormInputWrapper>

          <DoubleFormInputWrapper>
            <FormInputLabel htmlFor="password">Hasło</FormInputLabel>
            <ErrorText
              field="password"
              type="password"
              validate={value => isRequired(value, "Wymagane")}
              validateonchange="true"
              validateonblur="true"
              id="password"
              placeholder="Hasło"
            />
          </DoubleFormInputWrapper>
        </WrapDoubles>

        <ButtonWrapper>
          <Button type="submit">Dodaj</Button>
        </ButtonWrapper>
      </Form>
    );
  }
}

export default AddScannerForm;
