import React, { useEffect, useState } from 'react';
import { Header } from "../../../styles/styled-header";
import AdminWrapperComponent from "../../../components/AdminWrapperComponent";
import ScannerForm from "./ScannerForm";
import ScannersList from "./ScannerList";
import EventManagerService from "../../../services/EventManagerService";
import LoadingSpinner from "../../../components/LoadingSpinner";
import Swal from 'sweetalert2'

const ManagerScannersPage = ({ match }) => {
  const [scanners, setScanners] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const eventId = match.params.eventId;

  const fetchScanners = eventId => {
    EventManagerService.getScannersByEventId(eventId)
      .then(res => {
        setScanners(res.data.scanners);
        setLoading(false);
      })
      .catch(err => console.log('err', err))
  };

  useEffect(() => {
    if (eventId) {
      fetchScanners(eventId);
    }
  }, [eventId]);

  const deleteScanner = scannerId => {
    EventManagerService.removeScannerByEventIdByScannerId(eventId, scannerId)
      .then(res => {
        Swal('Sukces!', 'Skaner został usunięty', 'success');
        fetchScanners(eventId)
      })
      .catch(error => {
        Swal('Błąd!', 'Wystąpił błąd', 'error');
      })
  };

  return <AdminWrapperComponent>
    <Header>Zarządzaj skanerami</Header>
    {!isLoading ? <>
      <ScannersList scanners={scanners} deleteScanner={deleteScanner}/>
      <ScannerForm eventId={eventId} fetchScanners={fetchScanners}/>
    </> : <LoadingSpinner/>
    }
  </AdminWrapperComponent>
};

export default ManagerScannersPage;
