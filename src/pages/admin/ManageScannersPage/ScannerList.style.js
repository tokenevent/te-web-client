import styled from "styled-components";
// import { Button } from "../../../styles/styled-button";
  
export const Wrapper = styled.div`
    display: flex;
    flex-direction: column; 
    padding: 0 5%;
`;

export const ScannerRow = styled.div`
      display: flex;
      justify-content: space-between;
      margin-bottom: 5px;
`;
export const IconsWrapper = styled.div`
  display: flex;
`;

