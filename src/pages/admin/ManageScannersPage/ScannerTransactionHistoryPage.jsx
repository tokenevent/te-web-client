import React, { useEffect, useState } from 'react';
import { Header } from "../../../styles/styled-header";
import AdminWrapperComponent from "../../../components/AdminWrapperComponent";
import EventManagerService from "../../../services/EventManagerService";
import HistoryList from "./HistoryList";
import { Button } from "../../../styles/styled-button";
import LoadingSpinner from "../../../components/LoadingSpinner";
import NoTokenToDisplay from "../../../styles/styled-no-token-to-display";

const ScannerTransactionHistoryPage = ({ match }) => {
  const [history, setHistory] = useState([]);
  const [page, setPage] = useState(0);
  const [isButtonVisible, setButtonVisible] = useState(true);
  const [isLoading, setLoading] = useState(true);

  const scannerId = match.params.scannerId;

  const updateHistory = newPositions => {
    if (newPositions.length < 20) {

      setButtonVisible(false)
    }
    const result = history.concat(newPositions);
    setHistory(result);
  };

  const fetchTransactions = (scannerId, page) => {
    EventManagerService.getTransactionsByScannerId(scannerId, page)
      .then(res => {
        updateHistory(res.data.transactions);
        setLoading(false);
      })
      .catch(err => {
        console.log('err', err);
        setLoading(false);
      })
  };

  const updatePage = delta => {
    if (page + delta <= 0) {
      setPage(0)
    } else {
      setPage(page + delta)
    }
  };

  useEffect(() => {
    if (scannerId) {
      fetchTransactions(scannerId, page);
    }
  });

  return <AdminWrapperComponent>
    <Header>Historia transakcji</Header>
    {!isLoading ?
      history.length > 0 ?
        <>
          <HistoryList historyList={history}/>
          {isButtonVisible && <Button onClick={() => {
            updatePage(1)
          }}>Załaduj więcej</Button>}
        </> :
        <NoTokenToDisplay>Brak transakcji.</NoTokenToDisplay>
      : <LoadingSpinner/>}
  </AdminWrapperComponent>
};

export default ScannerTransactionHistoryPage;
