import React, { useState, useEffect} from 'react';
import { Header } from "../../../styles/styled-header";
import AdminWrapperComponent from "../../../components/AdminWrapperComponent";
import eventService from '../../../services/EventService';
import {countPLN} from '../../../utils/helpers';
import { primary } from '../../../styles/colors';
import styled from 'styled-components';
import {Form} from 'informed';
import {
  FormInputWrapper
} from '../../../styles/styled-login-register';
import {isRequired} from '../../../utils/validators';
import {ErrorText} from '../../../components/FormComponents/ErrorText';
import {DeleteButton, Button} from '../../../styles/styled-button';
import EventManagerService from "../../../services/EventManagerService";

const SingleToken = styled.div`
  border: 2px solid ${primary};
  border-radius: 5px;
  margin-bottom: 10px;
  display: flex;
  align-items: center;
  padding: 10px;
`;

const TokenInfoWrapper = styled.div`
  display: flex;
  width: 100%;
  margin-right: 10px;
  justify-content: space-between;
`;

const ManageBuyableTokensPage = ({ match }) => {
  const eventId = match.params.eventId;
  const [tokens, setTokens] = useState([]);
  const [formApi, setFormApi] = useState(null);
  const getFormApi = formApi => setFormApi(formApi);
  console.log(formApi);

  const addToken = (token) => {
    EventManagerService.eventAddBuyableTokens(eventId, token)
      .then(res => getTokens())
      .catch(err => console.log('err', err))
  };

  const removeToken = (tokenName) => {
    EventManagerService.eventRemoveBuyableToken(eventId, tokenName)
      .then(res => getTokens())
      .catch(err => console.log('err', err))
  }

  const getTokens = () => {
    eventService.getEvent(eventId)
      .then(res => setTokens(res.data.buyableTokens))
      .catch(error => console.log(error));
  }

  useEffect(() => {
    getTokens();
  });

  return <AdminWrapperComponent>
    <Header>Zarządzaj tokenami</Header>
    <h2>Lista tokenów:</h2>
    {tokens.map((token) => (
      <SingleToken key={token.id}>
        <TokenInfoWrapper>
          <div>{token.name}</div>
          <div>{countPLN(token.unitPrice)}</div>
        </TokenInfoWrapper>
        <DeleteButton onClick={() => removeToken(token.name)}>X</DeleteButton>
      </SingleToken>)
    )}
    <h2>Dodaj token:</h2>
    <Form onSubmit={addToken} getApi={getFormApi}>
      <FormInputWrapper>
        <label htmlFor="name">Nazwa Tokenu</label>
        <ErrorText
          field="name"
          validate={value => isRequired(value, "Wymagane")}
          validateonchange="true"
          validateonblur="true"
          id="name"
          placeholder="Nazwa Tokenu"
        />
      </FormInputWrapper>
      <FormInputWrapper>
        <label htmlFor="unitPrice">Cena Tokenu</label>
        <ErrorText
          field="unitPrice"
          validate={value => isRequired(value, "Wymagane")}
          validateonchange="true"
          validateonblur="true"
          id="unitPrice"
          placeholder="Cena Tokenu"
        />
      </FormInputWrapper>
      <Button type="submit">Dodaj</Button>
    </Form>

  </AdminWrapperComponent>
};

export default ManageBuyableTokensPage;
