import React, {Component} from 'react';
import Swal from 'sweetalert2';
import EventForm from '../../../components/Forms/AddEventForm/EventForm';
import eventService from '../../../services/EventService';
import AdminWrapperComponent from '../../../components/AdminWrapperComponent';
import { Header } from '../../../styles/styled-header';
import styled from 'styled-components';
import moment from 'moment';
import { checkLogedIn } from '../../../utils/helpers';

const EventFormWrapper = styled.div`
    padding: 0 5%;
`;

class AddEventPage extends Component {
    constructor(props) {
        super(props);
        this.addEvent = this.addEvent.bind(this);
        this.setFormApi = this.setFormApi.bind(this);
    }

    setFormApi = formApi => this.formApi = formApi;

    addEvent = vals => {
        vals.type = 'VALUE';
        vals.endingAt = moment(vals.endingAt).toISOString();
        vals.startingAt = moment(vals.startingAt).toISOString();
        eventService.addEvent(vals)
            .then(response => this.addEventSuccessHandler(response.data.response))
            .catch(error => this.addEventErrorHandler(error));
    };

    addEventSuccessHandler = respone => {
        Swal('Super!', 'Wydarzenie zostało dodane', 'success');
        this.resetForm();
    };

    addEventErrorHandler = error => {
        checkLogedIn(error);
        Swal('Oops...', 'Coś poszło nie tak!', 'error');
    };

    resetForm = () => this.formApi.reset();

    render() {
        return (
            <AdminWrapperComponent>
                <Header>Dodaj wydarzenie</Header>
                <EventFormWrapper>
                    <EventForm
                        onSubmit={this.addEvent}
                        getFormApi={this.setFormApi}
                    />
                </EventFormWrapper>
            </AdminWrapperComponent>
        );
    }
}

export default AddEventPage;
