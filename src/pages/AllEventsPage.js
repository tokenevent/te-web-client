import React, {Component} from 'react';
import Footer from './default/Footer';

import EventsListContainer from '../components/EventsListContainer';
import { Container } from '../styles/styled-container';

class AllEventsPage extends Component {
    render() {
        return (
            <div>
                <Container>
                    <EventsListContainer/>
                </Container>
                <Footer />
            </div>
        );
    }
}

export default AllEventsPage;
