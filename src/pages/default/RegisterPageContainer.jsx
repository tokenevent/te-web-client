import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {app} from './../../utils/enums';
import RegisterPage from './../RegisterPage';

class RegisterPageContainer extends React.Component {
    render() {
        return (
            <div>
                {localStorage.getItem('accessToken') ? <Redirect to={app.HOMEPAGE}/> :
                    <Route exact path={app.REGISTER_PAGE} component={RegisterPage}/>
                }
            </div>
        );
    }
}

export default RegisterPageContainer;