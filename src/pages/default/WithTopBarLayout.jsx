import NavBar from '../../components/NavBar/NavBar.jsx';

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { app } from './../../utils/enums';
import MainPage from './../MainPage';
import AllEventsPage from './../AllEventsPage';
import SingleEventPage from './../SingleEventPage';
import AddEventPage from '../admin/AddEventPage/AddEventPage';
import PrivateRoute from './../../components/PrivateRoute';
import RegisterPage from './../RegisterPage';
import BuyTokenPage from './../BuyTokenPage';
import UserTokensPage from './../UserTokensPage';
import TokensKindsPage from './../TokensKindsPage';
import GenerateTokensComponent from './../GeneratedTokensComponent';
import WalletPage from './../WalletPage';
import TokenHistoryPage from './../TokenHistoryPage';
import TransactionPage from './../TransactionPage';
import AdminPanelPage from '../AdminPanelPage/AdminPanelPage';
import AcceptRejectPage from './../AcceptRejectPage.jsx';
import ManagerScannersPage from "../admin/ManageScannersPage/ManageScannersPage";
import ScannerTransactionHistoryPage from "../admin/ManageScannersPage/ScannerTransactionHistoryPage";
import ManageBuyableTokensPage from "../admin/ManageBuyableTokensPage/ManageBuyableTokensPage";
import EventMoreInformationPage from "../admin/EventMoreInformationPage/EventMoreInformationPage";
import PayuGoBack from '../PayuGoBack';

class WithTopBarLayout extends React.Component {
  render() {
    return (
      <div>
        <NavBar/>
        <Switch>
          <Route exact path="/" component={MainPage}/>
          <Route exact path={app.REGISTER_PAGE} component={RegisterPage}/>
          <PrivateRoute exact path={app.ALL_EVENTS} component={AllEventsPage}/>
          <PrivateRoute exact path={app.ACCEPT_TRANS} component={AcceptRejectPage}/>
          <PrivateRoute exact path={app.TOKEN_TRANS} component={TransactionPage}/>
          <PrivateRoute exact path={app.SINGLE_EVENT} component={SingleEventPage}/>
          <PrivateRoute exact path={app.REJECT_TRANS} component={AcceptRejectPage}/>
          <PrivateRoute exact path={app.WALLET} component={WalletPage}/>
          <PrivateRoute exact path={app.USER_TOKENS} component={UserTokensPage}/>
          <PrivateRoute exact path={app.ADMIN_PANEL} component={AdminPanelPage}/>
          <PrivateRoute exact path={app.ADMIN_PANEL_ADD} component={AddEventPage}/>
          <PrivateRoute exact path={app.KINDS_TOKENS} component={TokensKindsPage}/>
          <PrivateRoute exact path={app.TOKEN_HISTORY} component={TokenHistoryPage}/>
          <PrivateRoute exact path={app.GENERATE_TOKENS} component={GenerateTokensComponent}/>
          <PrivateRoute exact path={app.BUY_TOKEN} component={BuyTokenPage}/>
          <PrivateRoute exact path={app.SCANNER_TRANSACTION_HISTORY} component={ScannerTransactionHistoryPage} />
          <PrivateRoute exact path={app.EVENT_SCANNERS} component={ManagerScannersPage}/>
          <PrivateRoute exact path={app.EVENT_BUYABLE_TOKENS} component={ManageBuyableTokensPage}/>
          <PrivateRoute exact path={app.PAYU_GO_BACK} component={PayuGoBack}/>
          <PrivateRoute exact path={app.EVENT_MORE_INFO} component={EventMoreInformationPage}/>
        </Switch>
      </div>
    );
  }
}

export default WithTopBarLayout;
