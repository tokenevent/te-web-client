import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {app} from './../../utils/enums';

import LoginPage from '../LoginPage';

class LoginPageContainer extends React.Component {
    render() {
        return (
            <div>
                {localStorage.getItem('accessToken') ? <Redirect to={app.HOMEPAGE}/> :
                    <div>
                        <Route exact path="/" render={() => <Redirect to="/login"/>}/>
                        <Route path="/login" component={LoginPage}/>
                    </div>
                }
            </div>
        );
    }
}

export default LoginPageContainer;