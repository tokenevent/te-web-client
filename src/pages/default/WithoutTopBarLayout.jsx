import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import {app} from './../../utils/enums';

import LoginPage from '../LoginPage';

class WithoutTopBarLayout extends React.Component {
    render() {
        return (
            <div>
                {localStorage.getItem('accessToken') ? <Redirect to={app.HOMEPAGE}/> :
                    <Switch>
                        <Route exact path="/" render={() => <Redirect to="/login"/>}/>
                        <Route path="/login" component={LoginPage}/>
                    </Switch>
                }
            </div>
        );
    }
}

export default WithoutTopBarLayout;