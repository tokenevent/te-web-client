import React from 'react';
import styled, { css } from 'styled-components';
import { Container } from '../../styles/styled-container';
import {Link} from 'react-router-dom';
import { Icomoon } from '../../styles/styled-ico';
import { mediaUp, mediaDown } from '../../styles/styled-breakpoints';

const FooterBar = styled.footer`
    background: #007bff;
    position:relative;
    margin-top: 75px;

    &:before {
        content:"";
        position:absolute;
        top:-60px;
        height:60px;
        left:0;
        right:0;
        background: linear-gradient(to bottom right, transparent 49%, #007bff 50%);
    }

    ${props => props.noskew && css`
        margin-top: 0;

        &:before {
            content: none;
            display: none;
        }
    `}
`;

const FooterSplitWrapper = styled.div`
    display: flex;

    ${ mediaDown.tablet`
        flex-direction: column;
    `}

    div {
        width: 100%;
        color: #fff;
        padding: 25px 0;

        ${ mediaUp.tablet`
            width: 50%;
        `}

        h3 {
            margin-top: 0;
            font-size: 24px;
            text-transform: uppercase;
        }

        ul {
            margin: 0;
            padding-left: 17px;
            
            li {
                margin-bottom: 10px;
                a {
                    color: #fff;
                    font-size: 14px;
                    text-decoration: none;

                    &:hover {
                        text-decoration: underline;
                    }
                }
            }
        }

        &:first-of-type {
            ul {
                display: flex;
                list-style: none;
                padding: 0;

                li {
                    margin-bottom: 5px;
                    margin-right: 10px;

                    a {
                        display: flex;
                        justify-content: center;
                        border: 1px solid #fff;
                        border-radius: 50%;
                        height: 40px;
                        width: 40px;
                        align-items: center;
                        text-decoration: none;
                        color: #fff;

                        i {
                            text-decoration: none;
                        }

                        &:hover {
                            background-color: #fff;
                            color: #007bff;
                        }
                    }
                }
            }
        }
    }
`;

class Footer extends React.Component {
    render() {
        return (
            <FooterBar noskew={!!this.props.noskew}>
                <Container>
                    <FooterSplitWrapper>
                        <div>
                            <h3>Social</h3>
                            <ul>
                                <li><Link to='/' > 
                                    <Icomoon Facebook />
                                </Link></li>
                                <li><Link to='/' > 
                                    <Icomoon Twitter />
                                </Link></li>
                                <li><Link to='/' > 
                                    <Icomoon Instagram />
                                </Link></li>
                            </ul>
                        </div>
                        <div>
                            <h3>Kontakt</h3>
                            <ul>
                                <li><Link to='/'>Warunki Użytkowania</Link></li>
                                <li><Link to='/'>Praca w EventToken</Link></li>
                                <li><Link to='/'>Skontaktuj się z nami</Link></li>
                            </ul>
                        </div>
                    </FooterSplitWrapper>
                </Container>
            </FooterBar>
        );
    }
}

export default Footer;