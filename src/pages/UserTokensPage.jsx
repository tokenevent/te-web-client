import React, {Component} from 'react';

import UserTokensComponent from '../components/UserTokensComponent';
import { Container } from '../styles/styled-container';
import { Header } from '../styles/styled-header';
import Footer from './default/Footer';

class UserTokensPage extends Component {
    render() {
        return (
            <div>
                <Container>
                    <Header>Twoje żetony</Header>
                    <UserTokensComponent />
                </Container>
                <Footer />
            </div>
        );
    }
}

export default UserTokensPage;
