import React, { Component } from 'react';
import Footer from './default/Footer';
import QRcode from 'qrcode.react';
import { Container } from '../styles/styled-container';
import { Header } from '../styles/styled-header';
import { ContainerCode } from "../styles/styled-containerCode";
import styled from 'styled-components';
import tokenService from '../services/TokenService';
import Swal from 'sweetalert2';
import LoadingSpinner from '../components/LoadingSpinner';
import { checkLogedIn } from '../utils/helpers';

const QrWrapper = styled.div`
    display: flex;
    justify-content: center;
    margin: 120px 0;
`;

class GeneratedTokensComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            containerId: null,
        }
    }

    componentDidMount = () => {
        let tokens = this.props.location.tokens;
        let tokensJson = JSON.stringify({ "tokens": tokens });
        tokenService.generateContainer(tokensJson)
            .then(response =>
                this.setState({
                    containerId: response.data.tokenContainerId
                })
            )
            .catch(error => {
                checkLogedIn(error);
                Swal('Oops...', 'Coś poszło nie tak!', 'error')
            });
    };

    render() {
        const { containerId } = this.state;
        return (
            <div>
                <Container>
                    { !containerId && <LoadingSpinner/>}
                    <Header>Twój kod </Header>
                    <ContainerCode>{containerId}</ContainerCode>
                    <QrWrapper>
                        {containerId &&
                        <QRcode value={containerId} renderAs={'svg'}/>
                        }

                    </QrWrapper>

                </Container>
                <Footer/>
            </div>
        );
    }
}

export default GeneratedTokensComponent;

