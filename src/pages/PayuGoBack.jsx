import React, { useEffect } from 'react';
import LoadingSpinner from '../components/LoadingSpinner';
import {app} from '../utils/enums';
import Swal from 'sweetalert2'

const PayuGoBack = ({ history}) => {

  useEffect(() => {
    Swal('Dodano Tokeny!', ' ', 'success');
    history.push(app.HOMEPAGE);
  }) 

  return <LoadingSpinner />
};

export default PayuGoBack;
