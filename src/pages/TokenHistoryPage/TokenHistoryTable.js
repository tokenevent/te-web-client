import React, { useState } from 'react';
import Table from "../../components/TransactionTable/TransactionTable";

const TokenHistoryTable = ({}) => {
  const [transactions, setTransactions] = useState([]);

  const columns = React.useMemo(
    () => [
      {
        Header: '',
        accessor: ''
      },
      {
        Header: 'Wydarzenie',
        accessor: 'eventName'
      },
      {
        Header: 'status',
        accessor: 'Status'
      },
      {
        Header: 'Data',
        accessor: 'statusChangedAt'
      },
    ],
    []
  );

  return <Table />
};

export default TokenHistoryTable;
