import React, {Component} from 'react';
import { Container } from '../styles/styled-container';
import Footer from './default/Footer';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';

import close from '../assets/images/close.png';
import success from '../assets/images/success.png';

const ContentWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

const Header = styled.h2`
    font-size: 32px;
    color: #000;
    margin: 20px auto;
`;

const AltText = styled.p`
    font-size: 14px;
    color: #888;
    margin-top: 0;
    margin-bottom: 15px;
`;

const GoBackButton = styled.a`
    display: block;
    background-color: #007bff;
    color: white;
    border: 1px solid #007bff;
    height: 40px;
    width: 100%;
    max-width: 240px;
    line-height: 40px;
    text-transform: uppercase;
    text-decoration: none;
    text-align: center;

    &:hover {
      color: #007bff;
      background-color: white;
    }
`;

class AcceptRejectPage extends Component {
    render() {
        let pageText = '',
        altText = '',
        addText = '',
        img = '';

        if(document.location.pathname === '/accepted') {
            pageText ='Sukces!';
            altText = 'Transakcja zakonczona pomyślnie.';
            addText = 'Mozesz odebrać swoje produkty.';
            img = success;
        } else if( document.location.pathname === '/rejected') {
            pageText = 'Anulowano';
            altText = 'Transakcja została anulowana.';
            img = close;
        }
        return (
            <div>
                <Container>
                    <ContentWrapper>
                        <img src={img} alt="Aceept/Reject icon"/>
                        <Header>{pageText}</Header>
                        <AltText>{altText}</AltText>
                        { addText === '' && <AltText>{addText}</AltText>}
                        <GoBackButton href="/wallet">Wróć</GoBackButton>
                    </ContentWrapper>
                </Container>
                <Footer />
            </div>
        );
    }
}

export default withRouter(AcceptRejectPage);
