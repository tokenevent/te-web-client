import React, { Component } from 'react';
import styled from 'styled-components';
import logo from '../assets/images/logo.png';

const NotFoundPage = styled.div`
    height: 100vh;
    width: 100vw;
    background: rgb(0,82,204);
    background: linear-gradient(333deg, rgba(0,82,204,1) 0%, rgba(2,68,145,1) 100%);
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 15px;
    color: #fff;
    font-size: 24px;
    text-transform: uppercase;

    img {
        margin-bottom: 50px;
    }
`;

class NotFound extends Component {
    render() {
        return (
            <NotFoundPage>
                <img src={logo} alt='' />
                Nie znaleziono strony
            </NotFoundPage>
        );
    }
}

export default NotFound;
