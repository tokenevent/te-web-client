import React, { Component } from 'react';
import Footer from './default/Footer';
import QRcode from 'qrcode.react';
import { Container } from '../styles/styled-container';
import { Header } from '../styles/styled-header';
import styled, {css} from 'styled-components';
import tokenService from '../services/TokenService';
import Swal from 'sweetalert2';
import { withRouter } from 'react-router-dom';
import { checkLogedIn } from '../utils/helpers';

const QrWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 20px 0;
`;

const Counter = styled.div`
    text-align: center;
    color: #777;
    font-size: 32px;
    font-weight: bold;

    ${props => props.visible && css`
        display: none;
    `}
`

const GetNewTokenButton = styled.button`
    background: transparent;
    border-radius: 3px;
    border: 2px solid #007bff;
    color: #007bff;
    padding: 10px 25px;
    margin: 20px auto;
    display: none;

    &:hover {
      background: #007bff;
      color: #fff;
    }

    ${props => props.visible && css`
        display: block;
    `}
`

const WalletCode = styled.div`
    color: #000;
    margin-top: 20px;
    font-size: 26px;
    text-align: center;
`

class WalletPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isWalletIdReturned: false,
            counterEnabled: true,
            buttonEnabled: false,
            qrEnabled: true,
            walletTest: true,
            walletId: null,
            validSeconds: null,
            transRedirect: false,
            transRes: null
        }
    }

    redirectTransaction = () => {
        tokenService.getTransaction()
            .then(res => this.getTransSuccessHandler(res))
            .catch(error => this.getTransErrorHandler(error))
    }

    renderCode = () => {
        tokenService.getWallet()
            .then(res => this.getUserSuccessHandler(res))
            .catch(error => this.getUserErrorHandler(error))
    }

    getTransSuccessHandler = res => {
        clearInterval(this.intervalHandle);
        this.props.history.push({
            pathname: `/transaction/${res.data.transaction.id}`,
            state: { 
                trans: res.data.transaction,
                transId: res.data.transaction.id
             }
        });
    }

    getTransErrorHandler = error => {
        console.log(error);
    }

    getUserSuccessHandler = res => {
        this.setState({
            walletId: res.data.id,
            validForSeconds: res.data.validForSeconds,
            isWalletIdReturned: true,
        });
        this.intervalHandle = setInterval(this.tick, 1000);
    };

    getUserErrorHandler = error => {
        checkLogedIn(error);
        this.setState({
            isWalletIdReturned: false,
        });
        Swal('Oops...', 'Coś poszło nie tak! Odswież stronę', 'error');
    };

    componentDidMount = () => {
        if(this.state.walletTest) {
            this.setState({
                walletTest: false
            })
            this.renderCode();
        }
    };

    componentWillUnmount = () => {
        clearInterval(this.intervalHandle);
    }

    tick = () => {
        if(this.state.validForSeconds %3 === 0) {
            this.redirectTransaction();
        }

        if (this.state.validForSeconds === 0) {
            this.setState({
                buttonEnabled: true,
                counterEnabled: false,
                qrEnabled: false
            })
            clearInterval(this.intervalHandle);
        } else {
            this.setState({
                validForSeconds: this.state.validForSeconds-1
            })
        }
    }

    generateAgain = () => {
        this.renderCode();
        if(this.state.isWalletIdReturned) {
            this.setState({
                buttonEnabled: false,
                counterEnabled: true,
                qrEnabled: true,
                secondsRemaining: 60,
            })
        } else {
            Swal('Oops...', 'Coś poszło nie tak! Odswież stronę', 'error');
        }
    }

    render() {
        const { walletId, validForSeconds, counterEnabled, buttonEnabled, qrEnabled} = this.state;
        return (
            <div>
                <Container>
                    <Header>Twój kod portfela</Header>
                    <QrWrapper style={{display: qrEnabled ? 'flex' : 'none' }}>
                        { walletId && 
                            <QRcode value={walletId} renderAs={'svg'} />
                        }
                        <WalletCode>{walletId}</WalletCode>
                    </QrWrapper>
                    <Counter {...(counterEnabled ? {} : {visible:true})}>
                        {validForSeconds !== undefined && validForSeconds + ' sekund'}
                    </Counter>
                    <GetNewTokenButton 
                        onClick={this.generateAgain}
                        {...(buttonEnabled && {visible:true})}>
                        Wygeneruj nowy kod
                    </GetNewTokenButton>
                </Container>
                <Footer />
            </div>
        );
    }
}

export default withRouter(WalletPage);

