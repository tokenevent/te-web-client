import React, { Component } from 'react';

import eventService from '../services/EventService';
import { Container } from '../styles/styled-container';
import SingleEvent from '../components/event/SingleEvent';
import LoadingSpinner from '../components/LoadingSpinner';
import { checkLogedIn } from '../utils/helpers';

class SingleEventPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataLoaded: false,
            event: {}
        };
    }

    getEvent = id => {
        eventService.getEvent(id)
            .then(response => this.setState({
                event: response.data,
                dataLoaded: true,
            }))
            .catch(error => checkLogedIn(error));
    };


    componentDidMount = () => {
        this.getEvent(this.props.match.params.id);
    };

    render() {
        const { dataLoaded } = this.state;
        return (
            <Container>
                {!dataLoaded && <LoadingSpinner/>}
                {dataLoaded && <SingleEvent event={this.state.event}/>}
            </Container>
        );
    }
}

export default SingleEventPage;
