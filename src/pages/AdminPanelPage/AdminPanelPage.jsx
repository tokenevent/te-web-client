import React, { Component } from 'react';
import { app } from '../../utils/enums';
import { checkAdmin, formatDate, checkLogedIn } from '../../utils/helpers';
import AdminWrapperComponent from '../../components/AdminWrapperComponent';
import { Header } from '../../styles/styled-header';
import eventService from '../../services/EventService'
import styled from 'styled-components';
import { primary } from '../../styles/colors';
import { LinkButton } from '../../styles/styled-button';
import LoadingSpinner from '../../components/LoadingSpinner';
import {mediaUp} from '../../styles/styled-breakpoints';

const EventListWrapper = styled.ul`
    padding: 0;
    margin: 0;
    list-style: none;
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const SingleEvent = styled.li`
    display: block;
    border: 2px solid ${primary};
    padding: 15px;
    width: 100%;
    border-radius: 5px;
    margin-bottom: 20px;

    h2 {
        margin-top: 0;
        color: ${primary};
    }
`;

const ButtonsWrapper = styled.div`
    margin-top: 20px;
    display: flex;
    flex-direction: column;

    a {
      margin-bottom: 15px;
    }

    ${ mediaUp.tablet`
      display: flex;
      flex-direction: row;
      justify-content: space-around;

      a {
        margin-bottom: 0;
      }
    `}
`;

const NoEventWrapper = styled.div`
    text-align: center;
`;

class AdminPanelPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      events: []
    };
  }

  componentDidMount() {
    eventService.getManagerEvents().then(
      res => {
        this.setState({ events: res.data.events, isLoading: false });
      })
      .catch((error) => {
        checkLogedIn(error);
        this.setState({ isLoading: false })
      });

    checkAdmin().then(res => {
      if (!res) {
        this.props.history.push(app.HOMEPAGE);
      }
    })
  }

  render() {
    const { events, isLoading } = this.state;
    return (
      <AdminWrapperComponent>
        <Header>Admin Panel</Header>
        {!isLoading ?
          <EventListWrapper>
            {!events.length && <NoEventWrapper>Nie masz wydarzeń do wyświetlenia.</NoEventWrapper>}
            {!!events.length && events.map((event) => {
              return <SingleEvent key={event.id}>
                <h2>{event.name}</h2>
                <div>Adres: {event.address.street}, {event.address.city}, {event.address.zipCode}</div>
                <div>Data startu: {formatDate(event.startingAt)}</div>
                <div>Data zakończenia: {formatDate(event.endingAt)}</div>
                <ButtonsWrapper>
                  <LinkButton href={`${app.ADMIN_PANEL}/${event.id}/eventmoreinformation`}>Wiecej informacji</LinkButton>
                  <LinkButton href={`${app.ADMIN_PANEL}/${event.id}/scanners`}>Zarządzaj skanerami</LinkButton>
                  <LinkButton href={`${app.ADMIN_PANEL}/${event.id}/buyabletokens`}>Zarządzaj tokenami</LinkButton>
                </ButtonsWrapper>
              </SingleEvent>
            })}
          </EventListWrapper>
          : <LoadingSpinner/>}
      </AdminWrapperComponent>
    );
  }
}

export default AdminPanelPage;
