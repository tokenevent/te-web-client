import React, {Component} from 'react';

import TokensKindsComponent from '../components/TokensKindsComponent';
import { Container } from '../styles/styled-container';
import { Header } from '../styles/styled-header';
import Footer from './default/Footer';

class UserTokensPage extends Component {
    render() {
        return (
            <div>
                <Container>
                    <Header>Twoje żetony</Header>
                    <TokensKindsComponent />
                </Container>
                <Footer />
            </div>
        );
    }
}

export default UserTokensPage;
