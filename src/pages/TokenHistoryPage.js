import React, { Component } from 'react';
import Footer from './default/Footer';
import styled from 'styled-components';
import tokenService from '../services/TokenService';
import userService from '../services/UserService';
import Swal from 'sweetalert2';
import { Header } from '../styles/styled-header';
import { Container } from '../styles/styled-container';
import LoadingSpinner from '../components/LoadingSpinner';
import { checkLogedIn, formatDate } from '../utils/helpers';
import { Button } from '../styles/styled-button';
import { mediaUp } from '../styles/styled-breakpoints';
import StatusCell from "../components/StatusCell/StatusCell";
import TransactionTable from "../components/TransactionTable/TransactionTable";
import NoTokenToDisplay from "../styles/styled-no-token-to-display";

// const TabWrapper = styled.div`
//     width: 100%;
//     max-width: 100%;
//     overflow-x: auto;
//     margin: 0 0 100px;
//     padding-bottom: 10px;
// `;

// const TransactionsTab = styled.table`
//     width: 100%;
//     text-align: center;
//     border-spacing: 0px;

//     th, td {
//         min-width: 100px;
//     }
// `;


const FilterButtonWrapper = styled.div`
    display: flex;
    flex-direction: column;

    button {
        width: 100%;
        margin: 10px 0;
        
    }

    ${mediaUp.phone`
        flex-direction: row;

        button {
            width: auto;
            margin: 20px 10px;
        }
    `}

`;


const transactionsColumns = [
  {
    Header: '',
    accessor: 'id',
    hideInMobile: true,
    Cell: ({ row }) => row.index + 1
  },
  {
    Header: 'Status',
    accessor: 'status',
    hideLabelInMobile: true,
    Cell: ({ row }) => <StatusCell status={row.original.status}/>,
  },
  {
    Header: 'Wydarzenie',
    accessor: 'eventName'
  },
  {
    Header: 'Data',
    accessor: 'statusChangedAt',
    Cell: ({ row }) => formatDate(row.original.statusChangedAt)
  },
  {
    Header: 'Zeskanował',
    accessor: 'scannerUserName',
  },
  {
    Header: 'Ilość',
    accessor: 'tokens.count'
  },
  {
    Header: 'Wartość',
    accessor: 'tokens.totalValue',
    Cell: ({ row }) => `${row.original.tokens.totalValue / 100}zł`
  },
];

const buyColumns = [
  {
    Header: '',
    accessor: 'id',
    hideInMobile: true,
    Cell: ({ row }) => row.index + 1
  },
  {
    Header: 'Status',
    accessor: 'status',
    hideLabelInMobile: true,
    Cell: ({ row }) => <StatusCell status={row.original.status}/>,
  },
  {
    Header: 'Wydarzenie',
    accessor: 'eventName'
  },
  {
    Header: 'Data',
    accessor: 'createdAt',
    Cell: ({ row }) => formatDate(row.original.createdAt)
  },
  {
    Header: 'Ilość',
    accessor: 'amount'
  },
  {
    Header: 'Wartość',
    accessor: 'totalValue',
    Cell: ({ row }) => `${row.original.totalValue / 100}zł`
  },
];

class TokenHistoryPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      renderTokens: [],
      tokenTransactionHistory: [],
      tokenBuyHistory: []
    }
  }

  buyHistoryHandler = res => {
    this.setState({
      tokenBuyHistory: res.data.list,
      isLoading: false,
    });
  };

  buyHistoryErrorHandler = error => {
    this.setState({
      isLoading: false,
    });
    Swal('Oops...', 'Nie udało się pobrać historii zakupów. Spróbuj ponownie za chwilę', 'error');
  };

  transactionHistoryHandler = res => {
    this.setState({
      renderTokens: res.data.transactions,
      tokenTransactionHistory: res.data.transactions,
      isLoading: false,
    });
  };
  transactionHistoryErrorHandler = error => {
    checkLogedIn(error);
    this.setState({
      isLoading: false,
    });
    Swal('Oops...', 'Nie udało się pobrać historii transakcji. Spróbuj ponownie za chwilę', 'error');
  };

  getAcceptedOnly = transaction => transaction.status === "ACCEPTED";

  getRejectedOnly = transaction => transaction.status === "REJECTED";

  componentDidMount = () => {
    tokenService.getTokenHistory()
      .then(res => this.transactionHistoryHandler(res))
      .catch(error => this.transactionHistoryErrorHandler(error));

    userService.getBuyHistory()
      .then(res => this.buyHistoryHandler(res))
      .catch(error => this.buyHistoryErrorHandler(error))
  };


  render() {
    const { isLoading, tokenTransactionHistory, tokenBuyHistory, renderTokens } = this.state;


    return (
      <div>
        {isLoading && <LoadingSpinner/>}
        <Container>
          <Header>Twoje transakcje</Header>
          {!!tokenTransactionHistory.length ? <><FilterButtonWrapper>
            <Button onClick={() => {
              this.setState({ renderTokens: tokenTransactionHistory });
            }}>
              Wszystkie
            </Button>
            <Button onClick={() => {
              this.setState({ renderTokens: tokenTransactionHistory.filter(this.getAcceptedOnly) })
            }}>
              Przyjęte
            </Button>
            <Button onClick={() => {
              this.setState({ renderTokens: tokenTransactionHistory.filter(this.getRejectedOnly) })
            }}>
              Odrzucone
            </Button>
          </FilterButtonWrapper>
            <TransactionTable key="scan" data={renderTokens} columns={transactionsColumns}/>
          </> : <NoTokenToDisplay>Przykro nam, nie masz jeszcze transakcji</NoTokenToDisplay>}


          <Header>Twoje zakupy</Header>
          {tokenBuyHistory && tokenBuyHistory.length > 0 ?
          <TransactionTable key="buy" data={tokenBuyHistory} columns={buyColumns} variant="buy-history"/>
          : <NoTokenToDisplay>Przykro nam, nie masz jeszcze transakcji</NoTokenToDisplay>
          }
        </Container>
        <Footer/>
      </div>
    );
  }
}

export default TokenHistoryPage;

