import React from 'react';
import {Form} from 'informed';
import Swal from 'sweetalert2';
import {withRouter} from 'react-router';

import {isRequired} from './../utils/validators';
import {ErrorText} from './../components/FormComponents/ErrorText';
import logo from './../assets/images/logoWhiteMedium.png';

import {app} from './../utils/enums';
import userService from './../services/UserService';
import {BAD_CREDENTIALS} from '../utils/messages';
import LoadingSpinner from '../components/LoadingSpinner';

import {
    ColorContainer,
    LogoWrapper,
    FormWrapper,
    FormInputWrapper,
    RedirectLinkWrapper,
    SubmitButton,
    SubmitErrorMessage,
} from '../styles/styled-login-register';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            badCredentials: false
        };
        this.processLogin = this.processLogin.bind(this);
        this.setFormApi = this.setFormApi.bind(this);
    }

    setFormApi = formApi => this.formApi = formApi;

    processLogin = (values) => {
        this.setState({ isLoading: true }, () => {
            userService.loginUser(values)
                .then(response => this.loginSuccessHandler(response))
                .catch(error => this.loginErrorHandler(error))
        });
    };

    loginSuccessHandler = response => {
        this.setState({badCredentials: false, isLoading: false})
        localStorage.setItem('accessToken', response.data);
        this.props.history.push(app.HOMEPAGE);
    };

    loginErrorHandler = error => {
        if(error.response.data !== null) {
            if (error.response.data.message === BAD_CREDENTIALS) {
                this.setState({badCredentials: true, isLoading: false});
            }
        } else {
            this.setState({ isLoading: false})
            Swal('Oops...', 'Coś poszło nie tak!', 'error')
        }
    };

    render() {
        const { isLoading } = this.state;
        return (
            <ColorContainer>
                <LogoWrapper>
                    <a href={app.HOMEPAGE}>
                        <img src={logo} alt="Tokenevent Logo" />
                    </a>
                </LogoWrapper>
                <FormWrapper>
                    <h1>Zaloguj się</h1>
                    <Form getApi={this.setFormApi} onSubmit={this.processLogin}>
                        { isLoading && <LoadingSpinner />}
                        <FormInputWrapper>
                            <label htmlFor="loginUsername">Nazwa użytkownika</label>
                            <ErrorText
                                field="username"
                                validate={value => isRequired(value, "* Wymagane pole")}
                                validateonchange="true"
                                validateonblur="true"
                                id="loginUsername"
                                placeholder="Nazwa użytkownika"
                            />
                        </FormInputWrapper>

                        <FormInputWrapper>
                            <label htmlFor="loginPassword">Hasło</label>
                            <ErrorText
                                field="password"
                                type="password"
                                validate={value => isRequired(value, "* Wymagane pole")}
                                validateonchange="true"
                                validateonblur="true"
                                id="loginPassword"
                                placeholder="Hasło"
                            />
                        </FormInputWrapper>
                        <SubmitButton type="submit">Zaloguj</SubmitButton>
                        {this.state.badCredentials ?
                            <SubmitErrorMessage>Wprowadzona nazwa użytkownika i/lub hasło jest nieprawidłowe</SubmitErrorMessage> : ''}
                    </Form>
                    <RedirectLinkWrapper>
                        Nie masz jeszcze konta? <a href={app.REGISTER_PAGE}>Zarejestruj się!</a>
                    </RedirectLinkWrapper>
                </FormWrapper>
            </ColorContainer>
        );
    }
}


export default withRouter(LoginPage);
