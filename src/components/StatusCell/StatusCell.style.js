import styled from "styled-components";

const Cell = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: 500;
  text-transform: uppercase;
  height: 70%; 
  text-align: center;
  color: white;
  background-color: ${props => props.color};
  border-radius: 5px;
  width: 100%;
`;

export default { Cell }
