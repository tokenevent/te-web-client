import React from 'react';
import { AcceptLightGrey, PendingOrange, PromotionPink, RejectLightRed } from "../../styles/colors";
import Styled from './StatusCell.style'
import transactionStatus from './../../utils/transactionStatus';

const StatusCell = ({ status }) => {

  const getStatusColor = s => {
    switch (s) {
      case transactionStatus.COMPLETED:
        return AcceptLightGrey;
      case transactionStatus.ACCEPTED:
        return AcceptLightGrey;
      case transactionStatus.REJECTED:
        return RejectLightRed;
      case transactionStatus.PENDING:
        return PendingOrange;
      case transactionStatus.PROMOTION:
        return PromotionPink;
      default:
        return "";
    } 
  };

  const getStatusLabel = s => {
    switch (s) {
      case transactionStatus.COMPLETED:
        return "Zakończona";
      case transactionStatus.ACCEPTED:
        return "Zaakceptowana";
      case transactionStatus.REJECTED:
        return "Odrzucona";
      case transactionStatus.PENDING:
        return "Oczekująca";
      case transactionStatus.PROMOTION:
        return "Kod promocyjny";
      default:
        return "";
    }
  };

  return <Styled.Cell color={getStatusColor(status)}>{getStatusLabel(status)}</Styled.Cell>
};

export default StatusCell;
