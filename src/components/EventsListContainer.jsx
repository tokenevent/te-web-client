import React from 'react';
import Swal from 'sweetalert2'
import styled from 'styled-components';

import eventService from './../services/EventService'
import EventBoxView from './../components/event/EventBoxView';
import { Header } from '../styles/styled-header';
import { mediaUp } from '../styles/styled-breakpoints';
import { checkLogedIn } from '../utils//helpers';
import LoadingSpinner from '../components/LoadingSpinner';

const EventsWrapper = styled.div`
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    width: 100%;

    ${mediaUp.desktop`
        flex-direction: row;
        max-width: 80%;
        margin: 15px auto;
    `}
`;

class EventsListContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            events: [],
        };
    }

    componentDidMount = () => this.getEvents();

    getEvents = () => {
        eventService.getEvents()
            .then(response => this.getEventsSuccessHandler(response))
            .catch(error => this.getEventsErrorHandler(error));
    };

    getEventsSuccessHandler = response => this.setState({ events: response.data, isLoading: false});
    getEventsErrorHandler = error => {
        checkLogedIn(error);
        this.setState({ isLoading: false});
        Swal('Oops...', 'Coś poszło nie tak!', 'error');
    }

    render() {
        const { events, isLoading } = this.state;
        return (
            <div>
                <Header>Lista wszystkich wydarzeń</Header>
                {isLoading && <LoadingSpinner/>}
                <EventsWrapper>
                    {events.map((event) =>
                        <EventBoxView
                            event={event}
                            eventId={event.id}
                            key={event.id}
                        />
                    )}
                </EventsWrapper>

            </div>
        );
    }
}

export default EventsListContainer;