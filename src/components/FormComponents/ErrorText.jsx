import React from 'react';
import {asField, BasicText} from 'informed';

export const ErrorText = asField(({fieldState, ...props}) => (
    <React.Fragment>
        <BasicText
            fieldState={fieldState}
            {...props}
            validateonchange="true"
            validateonblur="true"
            style={fieldState.error ? {border: 'solid 1px #f44336'} : undefined}
        />
        {fieldState.error ? (
            <div>{fieldState.error}</div>
        ) : undefined}
    </React.Fragment>
));