import {asField} from 'informed';
import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './../../styles/datepicker-custom.css';

export const ErrorDate = asField(({fieldState, fieldApi, ...props}) => {
    const {setValue, setTouched} = fieldApi;
    const {className} = props;
    const [startDate, setStartDate] = useState(null);

    const handleOnChange = (date) => {
        setValue(date);
        setStartDate(date);
    }

    const handleOnBlur = () => {
        setTouched();
    }

    return (
        <>
            <DatePicker
                selected={startDate}
                onChange={date => handleOnChange(date)}
                showTimeSelect
                timeFormat="HH:mm"
                timeIntervals={15}
                timeCaption="time"
                dateFormat="d MMMM yyyy, h:mm"
                placeholderText="Wybierz datę"
                onBlur={handleOnBlur}
                className={fieldState.error ? `${className} datepicker-error` : `${className}`}
            />
            {fieldState.error ? (
                <small style={{color: 'red'}}>{fieldState.error}</small>
            ) : null}
        </>
    );
});