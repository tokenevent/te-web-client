import React from 'react';
import { useExpanded, useTable } from 'react-table';
import { Heading, ReactTable, TableHead, Td, THead, Tr } from './Table.style';

const TransactionTable = ({ columns, data }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
      columns,
      data,
    },
    useExpanded);

  return <><ReactTable key={Math.round(1,1000)} {...getTableProps()}>
    <THead>
    {headerGroups.map(headerGroup => (
      <Heading {...headerGroup.getHeaderGroupProps()}>
        {headerGroup.headers.map(column => (
          <TableHead {...column.getHeaderProps()}>{column.render('Header')}</TableHead>
        ))}
      </Heading>
    ))}
    </THead>
    <tbody {...getTableBodyProps()}>
    {rows.map(
      (row, i) => {
        prepareRow(row);
        return (
          <>
            <Tr key={i} {...row.getRowProps()}>
              {row.cells.map((cell, cellIndex) => {
                return <><Td {...cell.getCellProps()} hideInMobile={columns[cellIndex].hideInMobile} data-label={columns[cellIndex].hideLabelInMobile === true ? '' : columns[cellIndex].Header}>{cell.render('Cell')}</Td></>
              })}
            </Tr>
          </>
        )
      }
    )}
    </tbody>
  </ReactTable>
  </>
};

export default React.memo(TransactionTable);
