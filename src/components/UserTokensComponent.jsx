import React from 'react'
import userService from '../services/UserService';
import Swal from 'sweetalert2';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import juweLogo from '../assets/images/juweLogo.jpg';
import { mediaUp } from '../styles/styled-breakpoints';
import LoadingSpinner from '../components/LoadingSpinner';
import { checkLogedIn } from '../utils/helpers';

const OwnedTokensWrapper = styled.div`
    font-size: 0;
    margin: 50px auto;
`;

const SingleTokenOwned = styled.div`
    width: calc(100% - 20px);
    display: inline-flex;
    font-size: 16px;
    margin: 0 10px 20px;
    border: 2px solid #007bff;
    padding: 15px;
    border-radius: 15px;

    ${ mediaUp.tablet`
        width: calc(50% - 30px);
    `}

    img {
        max-width: 100px;
        max-height: 100px;
        padding: 0;
        margin: 0;
        object-fit: contain;
    }

    div {
        display: flex;
        flex-direction: column;
        padding-left: 15px;
        width: 100%;

        h2 {
            font-size: 21px;
            color: #007bff;
            margin-top: 0;
            margin-bottom: 10px;
        }

        p {
            color: #777;
            margin-top: 0;
            margin-bottom: 10px;
        }

        a {
            align-self: flex-end;
            border: 2px solid #007bff;
            border-radius: 5px;
            color: #007bff;
            padding: 5px 15px;
            text-decoration: none;

            &:hover {
                background-color: #007bff;
                color: #fff;
            }
        }
    }
`;

const NoTokenToDisplay = styled.div`
    color: #999;
    font-size: 18px;
    text-align: center;
    margin-bottom: 130px;
`;

class UserTokensComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            events: [],
            isLoading: false
        }
    }

    componentDidMount = () => {
        this.setState({ isLoading: true });
        userService.getUser()
            .then(response => this.getUserSuccessHandler(response))
            .catch(error => this.getUserErrorHandler(error))
    };

    getUserSuccessHandler = response => {
        const events = response.data.events;
        events.map((event) => {
            if(event.tokens.length) {
                this.setState({ events: [...this.state.events,
                    {
                        eventName: event.event.name,
                        eventId: event.event.id,
                        tokensNumber: event.tokens.length,
                        eventTokens: event.tokens
                    }
                ]});
            }
            return 1;
        });
        this.setState({ isLoading: false });
    };

    getUserErrorHandler = error => {
        checkLogedIn(error);
        this.setState({ isLoading: false });
        Swal('Oops...', 'Coś poszło nie tak!', 'error')
    };

    render() {
        const { events, isLoading } = this.state;

        return (
            <div>
                {isLoading && <LoadingSpinner/>}
                <OwnedTokensWrapper>
                    { events.length && events.map((event) => (
                        <SingleTokenOwned key={event.eventId}>
                            <img src={juweLogo} alt="Event Thumbnail"/>
                            <div>
                                <h2>{event.eventName}</h2>
                                <p>{event.tokensNumber} żetonów</p>
                                <Link
                                    to={{
                                        pathname: 'tokens/'+ event.eventId
                                    }}>
                                Więcej</Link>

                            </div>
                        </SingleTokenOwned>
                    ))}

                    { !events.length && <NoTokenToDisplay>Nie posiadasz żetonów.</NoTokenToDisplay> }
                </OwnedTokensWrapper>
            </div>
        );
    }

}

export default UserTokensComponent;
