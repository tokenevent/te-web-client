import React from 'react';
import styled from 'styled-components';
import { mediaUp } from '../../../styles/styled-breakpoints';

const RowWrapper = styled.div `
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 15px;
    text-align: center;
`;

const TokenName = styled.div`
    color: #333;
    width: 30%;

    ${ mediaUp.tablet`
        width: 40%;
    `}
`;

const TokenPrice = styled.div`
    text-align: center;
    width: 30%;

    ${ mediaUp.tablet`
        width: 40%;
    `}
`;

const AddRemoveWrapper = styled.div `
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 40%;

    ${ mediaUp.tablet`
        width: 20%;
    `}

    span {
        font-weight: bold;
        color: #333;
    }

    button {
        color: #fff;
        border: 2px solid #007bff;
        background: #007bff;
        transition: all .3s ease-in-out;

        &:hover {
            ${ mediaUp.tablet`
                background: transparent;
                color: #007bff;
           `}
        }

    }
`;

class BuyTokenRow extends React.Component {
    render() {
        const {token, i, onChangeTokenAmount} = this.props;
        return (
            <RowWrapper key={token.id}>
                <TokenName>{token.name}</TokenName>
                <TokenPrice>{(token.unitPrice/100).toLocaleString('pl-PL', {
                    style:'currency',
                    currency:'PLN'
                })}</TokenPrice>
                <AddRemoveWrapper>
                    <button onClick={() => onChangeTokenAmount(i, -1)}>-</button>
                    <span>{token.amount}</span>
                    <button onClick={() => onChangeTokenAmount(i,1)}>+</button>
                </AddRemoveWrapper>
            </RowWrapper>
        );
    }
}

export default BuyTokenRow;
