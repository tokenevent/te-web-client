import React from 'react';

import {Form} from 'informed';
import BuyTokenRow from './BuyTokenRow'

class BuyTokenForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orderedTokens: [],
            allTokens: []
        }
    }

    // componentDidMount = () => this.prepareTokens(this.props.event.buyableTokens);

    // prepareTokens(tokens) {
    //     let listOfTokens = [];
    //     tokens.forEach(token => {
    //         let preparedToken = {
    //             id: token.id,
    //             maxQuantity: token.quantity,
    //             name: token.tokenKind.name,
    //             unitPrice: token.tokenKind.unitPrice,
    //             amount: 0
    //         };
    //         listOfTokens.push(preparedToken);
    //     });
    //     this.setState({allTokens: listOfTokens})
    // }

    // changeTokenAmount = (i, delta) => {
    //     this.setState(prevState => {
    //         const buyableTokens = [...prevState.allTokens];
    //         if ((buyableTokens[i].amount === 0 && delta < 0)) {
    //             delta = 0;
    //         }
    //         buyableTokens[i].amount += delta;
    //         return {
    //             buyableTokens
    //         };
    //     });

    // };

    render() {
        const {allTokens} = this.state;
        let suma = <>{this.state.allTokens.reduce(function (previousValue, currentValue, index, array) {
            return previousValue + (currentValue.unitPrice * currentValue.amount);
        }, 0)}</>;
        return (
            <Form>
                {allTokens.map((token, i) => (
                    <BuyTokenRow
                        key={token.id}
                        i={i}
                        token={token}
                        onChangeTokenAmount={this.changeTokenAmount}/>
                ))}
                <div>
                    <div>
                        Suma: {(suma/100).toLocaleString('pl-PL', {
                        style:'currency',
                        currency:'PLN'
                    })} PLN
                    </div>
                    <button onClick={() => this.props.onSubmit(this.state.allTokens)} className="btn btn-success">Kup
                    </button>
                </div>
            </Form>
        );
    }

}

export default BuyTokenForm;
