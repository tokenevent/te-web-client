import React from 'react';
import {Form} from 'informed';
import {isRequired} from './../../../utils/validators';
import {ErrorText} from './../../FormComponents/ErrorText';
import {ErrorDate} from './../../FormComponents/ErrorDate';
import styled from 'styled-components';
import { primary } from '../../../styles/colors';
import { Button } from '../../../styles/styled-button';

const FormInputWrapper = styled.div`
    display: flex;
    flex-direction: column;

    input {
        display: block;
        border: 1px solid ${primary};
        color: black;
        padding: 5px;
        width: 100%;
        padding: 10px;
        line-height: 1.4;

        &#date {
            width: 100%;
        }
    }

    .react-datepicker__input-container {
        width: 100%;
    }
`;

const WrapDoubles = styled.div`
    display: flex;
    justify-content: space-between;
`;

const DoubleFormInputWrapper = styled(FormInputWrapper)`
    width: 49%;
`;

const FormInputLabel = styled.label`
    display: block;
    color: ${primary};
    font-size: 12px;
    margin: 8px 0;
`;

const ButtonWrapper = styled.div`
    margin-top: 20px;
`

class EventForm extends React.Component {
    render() {
        const {onChange, onSubmit, getFormApi} = this.props;

        return (
            <Form onChange={onChange} onSubmit={onSubmit} getApi={getFormApi}>
                {/* change to select */}
                {/* <FormInputWrapper>
                    <FormInputLabel htmlFor="type">Typ</FormInputLabel>
                    <ErrorText
                        field="type"
                        validate={value => isRequired(value, "Wymagane")}
                        validateonchange="true"
                        validateonblur="true"
                        id="type"
                        placeholder="Typ"
                    />
                </FormInputWrapper> */}
                <FormInputWrapper>
                    <FormInputLabel htmlFor="name">Nazwa wydarzenia</FormInputLabel>
                    <ErrorText
                        field="name"
                        validate={value => isRequired(value, "Wymagane")}
                        validateonchange="true"
                        validateonblur="true"
                        id="name"
                        placeholder="Nazwa"
                    />
                </FormInputWrapper>

                <FormInputWrapper>
                    <FormInputLabel htmlFor="description">Opis wydarzenia</FormInputLabel>
                    <ErrorText
                        field="description"
                        validate={value => isRequired(value, "Wymagane")}
                        validateonchange="true"
                        validateonblur="true"
                        id="description"
                        placeholder="Opis"
                    />
                </FormInputWrapper>

                <WrapDoubles>
                    <DoubleFormInputWrapper>
                        <FormInputLabel htmlFor="city">Miasto</FormInputLabel>
                        <ErrorText
                            field="city"
                            validate={value => isRequired(value, "Wymagane")}
                            validateonchange="true"
                            validateonblur="true"
                            id="city"
                            placeholder="Miasto"
                        />
                    </DoubleFormInputWrapper>

                    <DoubleFormInputWrapper>
                        <FormInputLabel htmlFor="zipCode">Kod pocztowy</FormInputLabel>
                        <ErrorText
                            field="zipCode"
                            validate={value => isRequired(value, "Wymagane")}
                            validateonchange="true"
                            validateonblur="true"
                            id="zipCode"
                            placeholder="Kod pocztowy"
                        />
                    </DoubleFormInputWrapper>
                </WrapDoubles>

                <FormInputWrapper>
                    <FormInputLabel htmlFor="city">Ulica</FormInputLabel>
                    <ErrorText
                        field="street"
                        validate={value => isRequired(value, "Wymagane")}
                        validateonchange="true"
                        validateonblur="true"
                        id="street"
                        placeholder="Ulica"
                    />
                </FormInputWrapper>

                <WrapDoubles>
                    <DoubleFormInputWrapper>
                        <FormInputLabel htmlFor="startingAt">Data startu</FormInputLabel>
                        <ErrorDate
                            field="startingAt"
                            id="startingAt"
                            validate={value => isRequired(value, "Wymagane")}
                            validateonchange="true"
                            validateonblur="true"
                        />
                    </DoubleFormInputWrapper>

                    <DoubleFormInputWrapper>
                        <FormInputLabel htmlFor="endingAt">Data konca</FormInputLabel>
                        <ErrorDate
                            field="endingAt"
                            id="endingAt"
                            validate={value => isRequired(value, "Wymagane")}
                            validateonchange="true"
                            validateonblur="true"
                        />
                    </DoubleFormInputWrapper>
                </WrapDoubles>

                <ButtonWrapper>
                    <Button type="submit">Zapisz</Button>
                </ButtonWrapper>
            </Form>
        );
    }
}

export default EventForm;