import React from 'react';

import {Form} from 'informed';
import uuid from 'uuid'
import BuyableTokenRow from "./BuyableTokenRow";

class BuyableTokenForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {buyableTokens: []};
        this.save = this.save.bind(this);
        this.addBuyableTokenRow = this.addBuyableTokenRow.bind(this);
        this.removeBuyableTokenRow = this.removeBuyableTokenRow.bind(this);
    }

    addBuyableTokenRow() {
        this.setState(prevState => {
            return {
                buyableTokens: [...prevState.buyableTokens, {
                    key: uuid.v4(),
                    name: "",
                    unitPrice: "",
                    quantity: ""
                }]
            };
        });
    }

    removeBuyableTokenRow(i) {
        this.setState(prevState => {
            const buyableTokens = [...prevState.buyableTokens];
            buyableTokens.splice(i, 1);
            return {
                buyableTokens
            };
        });
    }

    save = tokens => this.props.onSubmit(tokens.buyableTokens);

    render() {
        return (
            <div>
                <button type="submit" onClick={this.addBuyableTokenRow}>+</button>
                <Form id="buyableTokens-form" onSubmit={this.save}>
                    {this.state.buyableTokens.map((token, i) => (
                        <BuyableTokenRow i={i} token={token} removeRow={this.removeBuyableTokenRow}/>
                    ))}
                    {this.state.buyableTokens.length > 0 &&
                    <button type="submit" className="btn btn-success">Dodaj</button>}
                </Form>

            </div>
        );
    }
}

export default BuyableTokenForm;