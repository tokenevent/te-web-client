import React from 'react';
import {ErrorText} from "../../FormComponents/ErrorText";
import {isRequired} from "../../../utils/validators";

class BuyableTokenRow extends React.Component {

    render() {
        const {i, token, removeRow} = this.props;
        return (
            <div key={`buyableToken-field-${token.key}`} className="form-group row">
                <div className="col-1">
                    <label htmlFor={`buyableToken-${i}`}>Nazwa:</label>
                </div>
                <div className="col-2">
                    <ErrorText
                        validate={value => isRequired(value, "Wymagane")}
                        className="form-control"
                        validateonchange="true"
                        validateonblur="true"
                        field={`buyableTokens[${i}].name`}
                        id={`buyableTokens-name-${i}`}
                        key={`buyableTokens-name-${token.key}`}
                    />
                </div>
                <div className="col-1">
                    <label htmlFor={`buyableToken-${i}`}>Cena jedn.</label>
                </div>
                <div className="col-2">
                    <ErrorText
                        validate={value => isRequired(value, "Wymagane")}
                        className="form-control"
                        validateonchange="true"
                        validateonblur="true"
                        field={`buyableTokens[${i}].unitPrice`}
                        id={`buyableTokens-unitPrice-${i}`}
                        key={`buyableTokens-unitPrice-${token.key}`}
                    />
                </div>
                {/*<div className="col-1">*/}
                    {/*<label htmlFor={`buyableToken-${i}`}>quantity:</label>*/}
                {/*</div>*/}
                {/*<div className="col-2">*/}
                    {/*<ErrorText*/}
                        {/*validate={value => isRequired(value, "Wymagane")}*/}
                        {/*className="form-control"*/}
                        {/*validateonchange="true"*/}
                        {/*validateonblur="true"*/}
                        {/*field={`buyableTokens[${i}].quantity`}*/}
                        {/*id={`buyableTokens-quantity-${i}`}*/}
                        {/*key={`buyableTokens-quantity-${token.key}`}*/}
                    {/*/>*/}
                {/*</div>*/}
                <div className="col-1">
                    <button className="btn btn-danger" onClick={() => removeRow(i)}>-</button>
                </div>
            </div>
        );
    }
}

export default BuyableTokenRow;