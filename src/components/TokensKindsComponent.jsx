import React from 'react'
import Swal from 'sweetalert2';
import LoadingSpinner from '../components/LoadingSpinner';
import { withRouter } from 'react-router-dom';
import { checkLogedIn, countPLN, groupBy } from '../utils/helpers';
import { SingleTokenWrapper, TokensWrapper } from '../styles/styled-token-kinds';
import userService from '../services/UserService';
import TokenKind from "./TokenKind/TokenKind";

class TokensKindsComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tokens: [],
      isLoading: true
    }
  }

  componentDidMount = () => {
    userService.getUser()
      .then(response => this.getUserSuccessHandler(response))
      .catch(error => this.getUserErrorHandler(error))
  };

  getUserSuccessHandler = response => {
    const events = response.data.events;
    const eventId = this.props.match.params.id;
    events.map((event) => {
      if (event.event.id === eventId) {
        if (event.tokens.length) {
          this.setState({
            tokens: groupBy(event.tokens, 'buyableTokenName'),
            isLoading: false
          });
        }
        ;
      }
      return '';
    });
  };

  getUserErrorHandler = error => {
    checkLogedIn(error);
    this.setState({ isLoading: false });
    Swal('Oops...', 'Coś poszło nie tak!', 'error')
  };

  render() {
    const { tokens, isLoading } = this.state;

    return (
      <div>
        {isLoading && <LoadingSpinner/>}
        <TokensWrapper>
          {Object.keys(tokens).map((tokenName) => {
            return (<SingleTokenWrapper key={tokenName}>
              <TokenKind
                name={tokenName}
                unitPrice={countPLN(tokens[tokenName][0].tokenValue)}
                quantity={tokens[tokenName].length}
                totalAmount={countPLN(tokens[tokenName].length * tokens[tokenName][0].tokenValue)}
              />
            </SingleTokenWrapper>)
          })}
        </TokensWrapper>
      </div>
    );
  }

}

export default withRouter(TokensKindsComponent);
