import React, { useState } from 'react';
import { useExpanded, useTable } from 'react-table';
import { Details, Heading, ReactTable, TableHead, Td, THead, Tr } from './TransactionTable.style';
import LoadingSpinner from "../LoadingSpinner";
import EventManagerService from "../../services/EventManagerService";
import Swal from 'sweetalert2';
import TransactionDetailsTable from "./TransactionDetailsTable";
import uuid from 'uuid/v4';

const TransactionTable = ({ columns, data, variant = 'transaction-history' }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
      columns,
      data,
    },
    useExpanded);

  const [expandedColumns, setExpandedColumns] = useState(data.map(o => false));
  const [expandedData, setExpandedData] = useState({});
  const [isLoading, setLoading] = useState(false);

  const fetchTransactionDetails = index => {
    if (variant === 'transaction-history') {
      EventManagerService.getTransactionDetailsByTransactionId(data[index].id)
        .then(resp => fetchTransactionDetailsSuccessHandler(resp, index))
        .catch(e => fetchTransactionDetailsErrorHandler(e, index));
    } else {
      EventManagerService.getBuyDetailsByTransactionId(data[index].id)
        .then(resp => fetchTransactionDetailsSuccessHandler(resp, index))
        .catch(e => fetchTransactionDetailsErrorHandler(e, index));
    }
  };

  const fetchTransactionDetailsSuccessHandler = (response, index) => {
    const copy = { ...expandedData };
    copy[index] = response.data;
    setExpandedData(copy);
    setLoading(false);
  };

  const fetchTransactionDetailsErrorHandler = (response, index) => {
    Swal('Oops...', 'Coś poszło nie tak!', 'error');
    let expanded = [...expandedColumns];
    expanded[index] = false;
    setExpandedColumns(expanded);
    setLoading(false);
  };

  const handleExpanded = index => {
    let expanded = [...expandedColumns];
    expanded[index] = !expandedColumns[index];
    setExpandedColumns(expanded);
    if (!expandedData[index]) {
      setLoading(true);
      fetchTransactionDetails(index);
    }
  };

  return <>
    {isLoading && <LoadingSpinner key={variant} />}
    <ReactTable {...getTableProps()}>
    <THead>
    {headerGroups.map(headerGroup => (
      <Heading {...headerGroup.getHeaderGroupProps()}>
        {headerGroup.headers.map(column => (
          <TableHead {...column.getHeaderProps()}>{column.render('Header')}</TableHead>
        ))}
      </Heading>
    ))}
    </THead>
    <tbody {...getTableBodyProps()}>
    {rows.map(
      (row, i) => {
        prepareRow(row);
        return (
          <>
            <Tr key={uuid()} {...row.getRowProps()} onClick={() => handleExpanded(i)}>
              {row.cells.map((cell, cellIndex) =>
                <Td key={cellIndex} {...cell.getCellProps()} hideInMobile={columns[cellIndex].hideInMobile} data-label={columns[cellIndex].hideLabelInMobile === true ? '' : columns[cellIndex].Header}>{cell.render('Cell')}</Td>
              )}
            </Tr>
            {expandedColumns[i] &&
            <tr key={uuid()}>
              <Details colSpan={columns.length}>
                {expandedData[i] &&
                  <TransactionDetailsTable startingIndex={i+1} data={expandedData[i].tokens}/>
                }
              </Details>
            </tr>
            }
          </>
        )
      }
    )}
    </tbody>
  </ReactTable>

  </>
};

export default React.memo(TransactionTable);
