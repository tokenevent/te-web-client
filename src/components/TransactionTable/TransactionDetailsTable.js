import React from 'react';
import Table from "../Table/Table";

const TransactionDetailsTable = ({ startingIndex, data }) => {
  
  const columns = React.useMemo(
    () => [
      {
        Header: '',
        accessor: 'index',
        hideInMobile: true,
        Cell: ({ row }) => `${startingIndex}.${row.index + 1}`
      },
      {
        Header: 'Nazwa żetonu',
        accessor: 'buyableTokenName',
      },
      {
        Header: 'Ilość',
        accessor: 'amount',
      },
      {
        Header: 'Cena jednostkowa',
        accessor: 'unitPrice',
        Cell: ({row}) => `${row.original.unitPrice/100}zł`
      },
      {
        Header: 'Suma',
        accessor: 'totalValue',
        Cell: ({row}) => `${row.original.totalValue/100}zł`
      },
    ],
    [startingIndex]
  );

  return <Table data={data} columns={columns}/>
};

export default TransactionDetailsTable;
