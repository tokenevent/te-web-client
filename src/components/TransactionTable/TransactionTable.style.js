import styled, { css } from 'styled-components';
import { primary } from '../../styles/colors';
import { mediaDown } from "../../styles/styled-breakpoints";

const ReactTable = styled.table`
    border-spacing: 0;
    width: 100%;
    ${ mediaDown.tablet`
       &:thead {
      border: none;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }
    `}
`;
const Tr = styled.tr`
   &:last-child {
     td {
         border-bottom: 0;
       }
    }
  cursor: pointer;
  :hover {
    background-color: rgba(0, 0, 0, 0.04);  
  }
  
  ${mediaDown.tablet`
    border-bottom: 3px solid #ddd;
    display: block;
    margin-bottom: .625em;
  `}
`;

const Th = styled.th`
  margin: 0;
  padding: 0.5rem;
     &:last-child {
        border-right: 0;
      }
`;

const Td = styled(Th)`
  height: 60px;
  font-weight: 500;
  border-bottom: 1px solid rgba(0, 0, 0, 0.15);
  
  ${mediaDown.tablet`
    border-bottom: 1px solid #ddd;
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: .8em;
    text-align: right;
    min-height: 30px;
      
    &:before {
     content: attr(data-label);
     float: left;
     font-weight: bold;
     text-transform: uppercase;
     }
     
     ${props => props.hideInMobile === true && css`
      display: none;
     `}
  `}
`;

const TableHead = styled(Th)`
 border-bottom: 1px solid ${primary};
`;

const Details = styled.td``;

const Heading = styled.tr`
  
`;

const THead = styled.thead`
${mediaDown.tablet`
  border: none;
  clip: rect(0 0 0 0);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
 `}
`;
export {
  ReactTable,
  Tr,
  Th,
  Td,
  TableHead,
  Details,
  Heading,
  THead
}
