import React from 'react';
import styled from 'styled-components';
import AdminSidebarComponent from './AdminSidebarComponent';
import { mediaUp } from '../styles/styled-breakpoints';

const AdminWrapper = styled.div`
    display: block;

    ${ mediaUp.desktop`
        position: fixed;
        bottom: 0;
        display: flex;
        height: calc(100vh - 70px);
        width: 100%;
    `}
`;

const AdminContentScroll = styled.div`
    height: 100%;
    overflow: auto;
`;

const AdminContent = styled.div`
    background-color: #fff;
    position: relative;
    width: 100%;
    overflow-x: visible;
    overflow-y: hidden;
    padding: 0 5%;

    ${ mediaUp.desktop`
        width: 75%;
    `}
`;

const AdminWrapperComponent = (props) => (
    <AdminWrapper>
        <AdminSidebarComponent />
        <AdminContent>
            <AdminContentScroll>
                {props.children}
            </AdminContentScroll>
        </AdminContent>
    </AdminWrapper>
);

export default AdminWrapperComponent;