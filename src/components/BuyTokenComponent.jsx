import React from 'react';
import styled from 'styled-components';
import BuyTokenRow from './Forms/BuyTokenForm/BuyTokenRow';
import tokenService from '../services/TokenService';
import Swal from 'sweetalert2'
import { Button } from '../styles/styled-button';
import LoadingSpinner from '../components/LoadingSpinner';
import { ErrorRed } from '../styles/colors';

const NoTokens = styled.div`
    width: 100%;
    text-align: center;
    font-size: 24px;
    color: #333;
`

const TotalPrice = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 35px;

    div {
        text-transform: uppercase;
        font-weight: bold;
        color: #007bff;

        span {
            font-size: 18px;
            color: #333;
        }
    }
`;

const TabWrapper = styled.div`
    max-width: 800px;
    margin: 90px auto;
`;

const TabHead = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    color: #007bff;
    padding-bottom: 15px;
    border-bottom: 1px solid #007bff;
    margin-bottom: 15px;
`;

const Error = styled.div`
    padding: 20px 0;
    text-align: center;
    color: ${ErrorRed};
`;


class BuyTokenComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            anyTokens: true,
            allTokens: [],
            isLoading: false,
            amountError: false
        }
    }

    componentDidMount = () => {
        if (this.props.event.buyableTokens.length) {
            this.prepareTokens(this.props.event.buyableTokens);
        } else {
            this.setState({
                anyTokens: false
            })
        }
    };

    prepareTokens(tokens) {
        let listOfTokens = [];
        tokens.forEach(token => {
            let preparedToken = {
                id: token.id,
                maxQuantity: token.quantity,
                name: token.name,
                unitPrice: token.unitPrice,
                amount: 0
            };
            listOfTokens.push(preparedToken);
        });
        this.setState({ allTokens: listOfTokens })
    }

    changeTokenAmount = (i, delta) => {
        this.setState(prevState => {
            const buyableTokens = [...prevState.allTokens];
            if ((buyableTokens[i].amount === 0 && delta < 0)) {
                delta = 0;
            }
            buyableTokens[i].amount += delta;
            return {
                buyableTokens
            };
        });
    };

    buyTokens = tokens => {
        this.setState({ isLoading: true, amountError: false });
        let tokensToBuy = [];
        let tokenError = false;
        tokens.forEach(token => {
            if (token.amount > 100) {
                this.setState({ amountError: true, isLoading: false })
                tokenError = true;
            };
            tokensToBuy.push({
                id: token.id,
                quantity: token.amount
            })
        });

        if (!tokenError) {
            let request = {
                eventId: this.props.event.id,
                tokensToBuy: tokensToBuy
            };
            tokenService.issueToken(request)
                .then((response) => {
                    return response.data.locationUrl
                })
                .then(locationUrl => {
                    this.setState({ isLoading: false });
                    window.location.replace(locationUrl)
                })
                .catch(error => {
                    this.issueTokenErrorHandler(error)
                })
        }
    };

    issueTokenSuccessHandler = response => Swal('Dobra robota!', 'Tokeny są już w Twoim portfelu', 'success');
    issueTokenErrorHandler = error => {
        this.setState({ isLoading: false });
        Swal('Oops...', 'Coś poszło nie tak!', 'error')
    };

    render() {
        const { anyTokens, allTokens, isLoading, amountError } = this.state;
        let cena = this.state.allTokens.reduce(function (previousValue, currentValue) {
            return previousValue + (currentValue.unitPrice * currentValue.amount);
        }, 0);
        return (

            <div>
                {isLoading && <LoadingSpinner/>}
                {
                    !anyTokens ?
                        <NoTokens>Niestety nie ma aktualnie tokenów na to wydarzenie. Wróć później!</NoTokens> : (
                            <TabWrapper>
                                <TabHead>
                                    <div>Nazwa:</div>
                                    <div>Cena:</div>
                                    <div>Dostępne:</div>
                                </TabHead>
                                {allTokens.map((token, i) => (
                                    <BuyTokenRow
                                        i={i}
                                        key={i+token}
                                        token={token}
                                        onChangeTokenAmount={this.changeTokenAmount}
                                    />
                                ))}
                                <TotalPrice>
                                    <div>
                                        Suma: <span>{
                                        (cena / 100).toLocaleString('pl-PL', {
                                            style: 'currency',
                                            currency: 'PLN'
                                        })
                                    }</span>
                                    </div>
                                    <Button onClick={() => {
                                        this.buyTokens(this.state.allTokens)
                                    }}>Kup</Button>
                                </TotalPrice>
                                { amountError && <Error>Maksymalna ilość żetonow dla jednej transakcji to 100</Error>}
                            </TabWrapper>
                        )
                }
            </div>
        );
    }
}

export default BuyTokenComponent;
