import React from 'react';
import coin from "../../assets/images/coin.png";
import {
  CoinAmountWrapper,
  CoinHeader,
  CoinImageWrapper,
  CoinInfoWrapper,
  CoinName,
  Pieces,
  Quantity,
  Slash, TotalAmount,
  UnitPrice,
  Wrapper
} from "./TokenKind.style";

const TokenKind = ({ name, unitPrice, quantity, totalAmount }) => {

  return <Wrapper>
    <CoinImageWrapper>
      <img src={coin} alt="Coin"/>
    </CoinImageWrapper>
    <CoinInfoWrapper>
      <CoinHeader>
        <CoinName>{name}</CoinName>
        <Slash>/</Slash>
        <UnitPrice>{unitPrice}</UnitPrice>
      </CoinHeader>
      <TotalAmount>{totalAmount}</TotalAmount>
    </CoinInfoWrapper>
    <CoinAmountWrapper>
      <Quantity>{quantity}</Quantity>
      <Pieces>szt.</Pieces>
    </CoinAmountWrapper>
  </Wrapper>
};

export default React.memo(TokenKind);
