import styled from 'styled-components';
import { mediaUp } from '../../styles/styled-breakpoints';

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  height: 100%;
`;

const CoinImageWrapper = styled.div`
  display: flex;
`;

const CoinInfoWrapper = styled.div`
  padding-left: 10px;
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  align-self: flex-start;
  height: 100%;
  justify-content: space-between;
`;

const CoinAmountWrapper = styled.div``;

const CoinHeader = styled.div`
  display: flex;
  align-items: center;
`;

const CoinName = styled.div`
  text-transform: uppercase;
  font-size: 20px;
  font-weight: 600;
`;

const Slash = styled.span`
  display: none;
  font-weight: 800;
  font-size: 21px;
  margin-left: 5px;
  margin-right: 5px;
  color: #aed5ff;

  ${mediaUp.phone`
    display: inline;
  `}
`;

const UnitPrice = styled.span`
  display: none;
  font-weight: 600;
  color: #9a9a9a;

  ${mediaUp.phone`
    display: inline;
  `}
`;

const Pieces = styled.span`
  font-weight: 500;
`;

const Quantity = styled.span`
  font-size: 36pt;
`;

const TotalAmount = styled.span`
  font-weight: 600;
`;

export {
  Wrapper,
  CoinImageWrapper,
  CoinInfoWrapper,
  CoinAmountWrapper,
  CoinName,
  CoinHeader,
  Slash,
  UnitPrice,
  Pieces,
  Quantity,
  TotalAmount
}
