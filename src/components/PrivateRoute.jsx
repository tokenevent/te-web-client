import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {app} from './../utils/enums';

export const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            localStorage.getItem('accessToken') ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: app.LOGIN_PAGE,
                        state: {from: props.location}
                    }}
                />
            )
        }
    />
);

export default PrivateRoute;