import React from 'react';
import styled from 'styled-components';
import { primary } from '../styles/colors';
import { app } from '../utils/enums';
import {Link} from 'react-router-dom';
import { mediaUp } from '../styles/styled-breakpoints';

const SideMenu = styled.nav`
    color: white;
    background-color: ${primary};
    width: 100%;
    padding: 25px 0;

    ${mediaUp.desktop`
      height: 100%;
      width: 25%;
    `}
`;

const SideMenuList = styled.ul`
    width: 100%;
    padding: 0;
    margin: 0;
    list-style: none;
    display: flex;
    flex-direction: column;
    align-items: center;

    li {
        a {
            color: white;
            text-transform: uppercase;
            text-decoration: none;
        }

        &:after {
            content: '';
            display: block;
            border-top: 1px solid white;
            width: 30px;
            margin: 35px auto;
        }

        &:last-of-type:after {
            display: none;
        }
    }
`;

const AdminSidebarComponent = () => (
  <SideMenu>
    <SideMenuList>
      <li><Link to={app.ADMIN_PANEL}>Twoje wydarzenia</Link></li>
      <li><Link to={app.ADMIN_PANEL_ADD}>Dodaj wydarzenie</Link></li>
    </SideMenuList>
  </SideMenu>
);

export default AdminSidebarComponent;