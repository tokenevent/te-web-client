import React from 'react';
import { LoadingInner, LoadingWrapper } from '../styles/styled-loader';

function LoadingSpinner() {
  return <LoadingWrapper><LoadingInner/></LoadingWrapper>;
}

export default LoadingSpinner;