import React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';
import { mediaUp } from '../../styles/styled-breakpoints';
import juweLogo from './../../assets/images/juweLogo.jpg';
import { formatDate } from '../../utils/helpers';

const SingleEventLinkWrapper = styled.div`
    width: 100%;
    padding: 0 15px;
    margin-bottom: 20px;

    ${ mediaUp.desktop`
        width: 50%;
    `}

    img {
        width: 100%;
        height: auto;
        border-radius: 10px;
    }

    a {
        text-decoration: none;
        cursor: pointer;
        
        .image-wrapper {
                position: relative;
                font-size: 0;
                
                .overlay {
                    position: absolute;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    font-size: 40px;
                    top: 0;
                    left: 0;
                    border-radius: 10px;
                    width: 100%;
                    height: 100%;
                    color: #fff;
                    background: rgba(0, 0, 0, .6);
                    opacity: 0;
                    transition: opacity .3s ease-in-out;
                }
            }

        &:hover {
            .overlay {
                opacity: 1;
            }

            h2 {
                color: #007bff;
            }
        }
    }
`;

const ShortDesc = styled.div`
    padding: 5px 0;

    span {
        color: #555;
        font-size: 14px;
    }

    span:first-of-type {
        &:after {
            content: '';
            border-left: 1px solid #555;
            margin: 0 10px;
            height: 5px;
        }
    }

    h2 {
        color: #111;
        font-size: 21px;
        margin-top: 10px;
        margin-bottom: 0;
    }
`;

class EventBoxView extends React.Component {
    render() {
        const {event} = this.props;
        return (
            <SingleEventLinkWrapper>
                <Link to={`/event/${event.id}`}>
                    <div className="image-wrapper">
                        <div className="overlay">
                            Zobacz więcej
                        </div>
                        <img
                            src={juweLogo}
                            alt='event card'
                        />
                    </div>
                    <ShortDesc>
                        <span>{formatDate(event.startingAt)}</span>
                        <span>{event.address.city}, {event.address.street}</span>
                        <h2>{event.name}</h2>
                    </ShortDesc>
                </Link>

                {/* <div className="card-footer">
                    <button type="button" className="btn btn-primary mr-3"
                            onClick={() => onOpenBuyableTokensPopup(event)}>Dodaj żetony
                    </button>
                    <button type="button" className="btn btn-warning mr-3"
                            onClick={() => onOpenTokensPopup(event)}>Kup żetony
                    </button>
                    <button type="button" className="btn btn-warning mr-3"
                            onClick={() => onEditEvent(event)}>Edytuj
                    </button>
                    <button type="button" className="btn btn-danger"
                            onClick={() => onRemoveEvent(event.id)}>Usuń
                    </button>
                </div> */}
            </SingleEventLinkWrapper>
        );
    }
}

export default EventBoxView