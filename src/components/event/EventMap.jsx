import React from 'react'
import {Map as LeafletMap, Marker, TileLayer, Tooltip} from 'react-leaflet';

class EventMap extends React.Component {
    render() {
        const {lat, lon, eventName} = this.props;
        return (
            <LeafletMap
                center={[lat, lon]}
                zoom={14}
                maxZoom={18}
                attributionControl="true"
                zoomControl="true"
                doubleClickZoom="true"
                scrollWheelZoom="true"
                dragging="true"
                animate="true"
                easeLinearity={0.35}>
                <TileLayer
                    url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                />
                <Marker position={[lat, lon]}>
                    <Tooltip direction='top' opacity={0.8} permanent>
                        {eventName}
                    </Tooltip>
                </Marker>
            </LeafletMap>
        );
    }
}

export default EventMap