import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import { mediaUp } from '../../styles/styled-breakpoints';
import EventMap from './EventMap';
import Footer from '../../pages/default/Footer';
import { RowFull } from '../../styles/styled-row-full';
import { Button } from '../../styles/styled-button';
import { formatDate } from '../../utils/helpers';

import { Header } from '../../styles/styled-header';
import juweLogo from './../../assets/images/juweLogo.jpg';

const InfoBar = styled.ul`
    padding: 0;
    margin: 0;
    list-style: none;

    li {
        ${ mediaUp.desktop`
            display: inline-block;
            margin-right: 10px;

            &:after {
                content: '';
                border-left: 1px solid #aeaeae;
                display: inline-block;
                height: 10px;
                margin-left: 10px;
            }

            &:last-of-type:after {
                display: none;
            }
    `}
    }
`;

const DescWrapper = styled.p`
    display: flex;
    flex-direction: column;
    font-size: 16px;
    color: #111;
    margin-bottom: 50px;

    img {
        margin-bottom: 20px;
        object-fit: contain;
    }

    ${ mediaUp.desktop`
        display: block;
        float: left;

        img {
            float: left;
            margin-right: 20px;
        }

        &: after {
            content: '';
            clear: both;
        }
    `}
`;

const BuyTokensWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

class SingleEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isPopupOpen: false,
            isTokensPopupOpen: false,
            isBuyableTokensPopupOpen: false,
        };
    }

    render() {
        const buyTokenLink = {
            pathname: "/buy-token",
            event: this.props.event
        };

        return (
            <div>
                <Header>{this.props.event.name}</Header>
                <BuyTokensWrapper>
                    <InfoBar>
                        <li>Miasto: {this.props.event.address.city},
                        {this.props.event.address.zipCode} </li>
                        <li>Ulica: {this.props.event.address.street}</li>
                        <li>Data: {formatDate(this.props.event.startingAt)}</li>
                    </InfoBar>
                    <Link to={buyTokenLink}>
                        <Button>Kup żetony</Button>
                    </Link>
                </BuyTokensWrapper>

                <DescWrapper>
                    <img
                        src={juweLogo}
                        alt='event card'
                    />
                    {this.props.event.description}
                </DescWrapper>
                <RowFull>
                    {
                        <EventMap
                            lat={this.props.event.address.coordinates.lat}
                            lon={this.props.event.address.coordinates.lon}
                            eventName={this.props.event.name}
                        />
                    }
                    <Footer noskew />
                </RowFull>
            </div>
        );
    }
}

export default SingleEvent;
