import React, {Component} from 'react';
import RenderMenuList from './RenderMenuList';
import styled from 'styled-components'
import { mediaUp } from '../../styles/styled-breakpoints';
import { checkAdmin } from '../../utils/helpers';

const MobileMenuWrapper = styled.div `
    display: block;
    position: relative;

    ${mediaUp.desktop`display: none;`}
`;

const Overlay = styled.div `
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: fixed;
    top: 0;
    left: 0;
    overflow: hidden;
    height: ${props => (props.isOpen ? '100%;' : '0%;')};
    width: 100%;
    background: #fff;
    transition: height .3s ease-in-out;
    z-index: 9999;
`;

const OpenClose = styled.button `
    background: transparent;
    border: 0;
    padding: 0;
    width: 30px;
    height: 30px;
    position: absolute;
    right: 20px;
    top: -15px;
    z-index: 99999;

    &:focus {
        outline: none;
    }

    span {
        background: #007bff;
        position: absolute;
        width: 30px;
        height: 2px;
        left: 0;
        transition: transform .3s ease-in-out, opacity .2s ease-in-out;

        &:first-of-type {
            top: 5px;


            transform: ${props => (props.isOpen ? 'translateY(10px) rotate(135deg);' : 'none;')};
        }

        &:nth-of-type(2) {
            top: 15px;

            opacity: ${props => (props.isOpen ? '0;' : '1;')};
        }

        &:last-of-type {
            top: 25px;

            transform: ${props => (props.isOpen ? 'translateY(-10px) rotate(-135deg);' : 'none;')};
        }
    }
`;



class MobileMenu extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isOpen: false,
            isAdmin: null
        };
    }

    componentDidMount = () => {
        if(localStorage.getItem('accessToken')) {
            checkAdmin().then(res => {
                this.setState({ isAdmin: res});
            })
        };
    };

    toggleMenu() {
        this.setState({ isOpen: !this.state.isOpen });
        document.body.classList.toggle('scroll-lock');
    }

    closeMenu() {
        this.setState({ isOpen: false });
        document.body.classList.remove('scroll-lock');
    }

    render() {
        return (
            <MobileMenuWrapper>
                <Overlay isOpen={this.state.isOpen}>
                    <RenderMenuList
                        isAdmin={this.state.isAdmin}
                        closeMenu={() => this.closeMenu()}
                    />
                </Overlay>
                <OpenClose isOpen={this.state.isOpen} onClick={() => this.toggleMenu()}>
                    <span></span>
                    <span></span>
                    <span></span>
                </OpenClose>
            </MobileMenuWrapper>
        );
    }
}

export default MobileMenu;