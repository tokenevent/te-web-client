import React from 'react';
import {Link} from 'react-router-dom';
import {app} from '../../utils/enums';
import { Button } from '../../styles/styled-button';
import styled from 'styled-components';
import { primary} from '../../styles/colors';

import { mediaDown } from '../../styles/styled-breakpoints';

const NavLinksWrapper = styled.div `
    display: flex;
    justify-content: space-between;
    
    ${mediaDown.desktop`
        flex-direction: column;
        align-items: center;
    `}
`;

const ButtonsWrapper = styled.div `
    display: flex;
    align-items: center;

    button:last-of-type {
        margin-left: 5px;

        ${mediaDown.desktop`
            margin-left: 0;
            margin-bottom: 20px;
        `}
    }

    ${mediaDown.desktop`
        flex-direction: column;
    `}
`;

const MenuList = styled.ul `
    display: flex;
    justify-content: space-between;
    align-items: center;
    list-style: none;
    text-align: center;
    padding: 15px 30px;
    margin: 0;

    li {
        font-size: 14px;

        a {
            color: #007bff;
            text-decoration: none;
            font-weight: bold;
            text-transform: uppercase;
            letter-spacing: .5px;
            padding: 0 15px;
            
            &:hover {
                color: #0561c3;
            }
        }
    }

    ${mediaDown.desktop`
        flex-direction: column;
        align-items: center;
        padding: 0;

        li {
            margin-bottom: 20px;
            font-size: 16px;
        }
    `}
`;

const AdminPanelButton = styled(Link)`
    display: inline-block;
    border: 2px solid ${primary};
    color: #fff!important;
    background: ${primary};
    border-radius: 3px;
    padding: 5px 15px;
    text-align: center;
`;

const renderUserLoggedOutMenu = (props) => {
    return (
        <NavLinksWrapper>
            <MenuList>
                <li>
                    <Link to={app.HOMEPAGE} onClick={props.closeMenu}>Strona Główna</Link>
                </li>
            </MenuList>
            <ButtonsWrapper>
                <Link to={app.REGISTER_PAGE} onClick={props.closeMenu}>
                    <Button primary>Zarejestruj się</Button>
                </Link>
                <Link to={app.LOGIN_PAGE} onClick={props.closeMenu}>
                    <Button>Zaloguj się</Button>
                </Link>
            </ButtonsWrapper>
        </NavLinksWrapper>
    )
};

const renderUserLoggedInMenu = (props) => {
    return (
        <NavLinksWrapper>
            <MenuList>
                <li><Link to={app.HOMEPAGE} onClick={props.closeMenu}>Strona Główna</Link></li>
                <li><Link to={app.ALL_EVENTS} onClick={props.closeMenu}>Wydarzenia</Link></li>
                <li><Link to={app.USER_TOKENS} onClick={props.closeMenu}>Portfel</Link></li>
                <li><Link to={app.WALLET} onClick={props.closeMenu}>Kod Portfela</Link></li>
                <li><Link to={app.TOKEN_HISTORY} onClick={props.closeMenu}>Historia</Link></li>
                {props.isAdmin && <li><AdminPanelButton to={app.ADMIN_PANEL} onClick={props.closeMenu}>Panel Admina</AdminPanelButton></li>}
            </MenuList>
            <Link to={app.HOMEPAGE} onClick={props.closeMenu}>
                <Button onClick={() => {
                    localStorage.setItem('accessToken', '');
                }}>Wyloguj się</Button>
            </Link>
        </NavLinksWrapper>
    )
};

const RenderMenuList = (props) => (
    <div>
        {localStorage.getItem('accessToken') ? renderUserLoggedInMenu(props) : renderUserLoggedOutMenu(props)}
    </div>
);

export default RenderMenuList;