import React, {Component} from 'react';
import RenderMenuList from './RenderMenuList.jsx';
import styled from 'styled-components'
import { mediaDown } from '../../styles/styled-breakpoints';
import { checkAdmin } from '../../utils/helpers';

const NormalMenuWrapper = styled.div `
    display: block;
    width: 100%;

    ${mediaDown.desktop`display: none;`}

    > div {
        width: 100%;
    }
`;

class NormalMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAdmin: null,
        }
    }

    componentDidMount = () => {
        if(localStorage.getItem('accessToken')) {
            checkAdmin().then(res => {
                this.setState({ isAdmin: res});
            })
        };
    };

    render() {
        return (
            <NormalMenuWrapper>
                <RenderMenuList isAdmin={this.state.isAdmin}/>
            </NormalMenuWrapper>
        );
    }
}

export default NormalMenu;