import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components'
import {app} from '../../utils/enums';

import NormalMenu from './NormalMenu.jsx';
import MobileMenu from './MobileMenu.jsx';
import { Container } from '../../styles/styled-container';
import { mediaDown } from '../../styles/styled-breakpoints';

import logo from './../../assets/images/logo2.png';

const NavBarBrand = styled.div`
    padding: 15px 0;

    img {
        max-width: 100%;
    }
`;

const NavBarWrapper = styled.nav`
    display: flex;
    align-items: center;
    background-color: #fff;

    ${mediaDown.desktop`justify-content: space-between;`}
`;

class NavBar extends Component {

    render() {
        return (
            <Container>
                <NavBarWrapper>
                    <NavBarBrand>
                        <Link to={app.HOMEPAGE}>
                            <img src={logo} alt="Token Event Logo"/>
                        </Link>
                    </NavBarBrand>
                    <NormalMenu />
                    <MobileMenu />
                </NavBarWrapper>
            </Container>
        );
    }
}

export default NavBar;