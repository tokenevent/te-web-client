import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Provider} from 'react-redux'
import {app} from './utils/enums';

import './styles/App.css';
import registerServiceWorker from './registerServiceWorker';
import WithTopBarLayout from './pages/default/WithTopBarLayout';
import RegisterPageContainer from './pages/default/RegisterPageContainer';
import LoginPageContainer from './pages/default/LoginPageContainer';

import {store} from './store/index';

import 'normalize.css';

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Switch>
                <Route exact path="/login" component={LoginPageContainer}/>
                <Route exact path={app.REGISTER_PAGE} component={RegisterPageContainer}/>
                <Route component={WithTopBarLayout}/>
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();
