export const isRequired = (value, errorMessage) => {
    return !value || value === null || value === undefined ? errorMessage : undefined;
};

export const isEmail = (value, errorMessage) => {
    // eslint-disable-next-line
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) ? undefined : errorMessage
};

export const isPassword = (value, errorMessage) => {
    // eslint-disable-next-line
    return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(value) ? undefined : errorMessage;
};