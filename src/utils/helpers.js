import userService from '../services/UserService';

export function groupBy(list, props) {
  return list.reduce((a, b) => {
     (a[b[props]] = a[b[props]] || []).push(b);
     return a;
  }, {});
}

export function countPLN(value) {
  return (value / 100).toLocaleString('pl-PL', {
    style: 'currency',
    currency: 'PLN'
  });
}

export function formatDate(date) {
  const pre = new Date(date);
  return new Intl.DateTimeFormat('pl-PL', {
    year: 'numeric',
    month: 'long',
    day: '2-digit',
    hour: 'numeric',
    minute: 'numeric',
  }).format(pre)
}

export function checkAdmin() {
  return userService.getUser()
  .then(res => {
    return !!res.data.user.authorities.find(x => x.authority === "ROLE_EVENT_MANAGER");
  })
  .catch(error => {return error});
}

export function checkLogedIn(error) {
  if(error.response.data !== null) {
    if(error.response.status === 401) {
      localStorage.removeItem('accessToken');
      window.location.href = "/login";
    }
  }
}
