export const USERNAME_TAKEN = "Username is taken";
export const BAD_CREDENTIALS = "Bad login and/or password";
export const BAD_PROMO_CODE = "Promo code does not exist or is inactive.";