const transactionStatus = Object.freeze({
  ACCEPTED: 'ACCEPTED',
  REJECTED: 'REJECTED',
  PENDING: 'PENDING',
  COMPLETED: 'COMPLETED',
  PROMOTION: 'PROMOTION'
});
export default transactionStatus
