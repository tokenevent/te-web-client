import axios from 'axios';

export const authorizeRequest = () => {
    let token = localStorage.getItem('accessToken')
    return axios.create({
        timeout: 10000,
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
    });
};
