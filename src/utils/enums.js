const apiUrl = "https://tokeneventapp.herokuapp.com";

export const api = {
  EVENTS: apiUrl + "/events",
  MANAGER_EVENTS: apiUrl + "/events-manager/my/events",
  EVENTS_MANAGER: apiUrl + "/events-manager/events",
  USER_LOGIN: apiUrl + "/public/users/login",
  USER_REGISTER: apiUrl + "/public/users/register",
  ISSUE_TOKEN: apiUrl + "/tokens/payu",
  CURRENT_USER: apiUrl + "/users/me",
  GENERATE_CONTAINER: apiUrl + "/tokens/container",
  WALLET: apiUrl + "/users/me/wallet",
  TRANSACTION: apiUrl + "/my/transactions",
  TRANSACTION_HISTORY: apiUrl + "/my/transactions-history",
  BUY_HISTORY: apiUrl + "/my/buy-token-history",
  EVENT_SCANNERS: eventId => `${apiUrl}/events-manager/events/${eventId}/scanners`,
  SCANNER_TRANSACTION_HISTORY: (scannerId, page) => `${apiUrl}/events-manager/transactions-history/${scannerId}?page=${page}`,
  REMOVE_SCANNER_EVENT: (eventId, scannerId) => `${apiUrl}/events-manager/events/${eventId}/scanners/${scannerId}`,
  EVENT_ADD_BUYABLE_TOKEN: (eventId) => `${apiUrl}/events-manager/events/${eventId}/buyable-tokens`,
  EVENT_REMOVE_BUYABLE_TOKEN: (eventId, token) => `${apiUrl}/events-manager/events/${eventId}/buyable-tokens/${token}`,
  GET_SCANNER_TRANSACTION_DETAILS: (transactionId) => `${apiUrl}/transaction-details/${transactionId}`,
  GET_BUY_DETAILS: (transactionId) => `${apiUrl}/my/buy-token-history/${transactionId}/details`,
};

export const app = {
  HOMEPAGE: "/",
  ALL_EVENTS: "/events",
  SINGLE_EVENT: "/event/:id",
  WALLET: "/wallet",
  BUY_TOKEN: "/buy-token",
  ADD_EVENT: "/add-event",
  ADMIN_PANEL: "/admin-panel",
  ADMIN_PANEL_ALL: "/admin-panel/all",
  ADMIN_PANEL_ADD: "/admin-panel/add",
  ADMIN_PANEL_EDIT: "/admin-panel/edit/:id",
  LOGIN_PAGE: "/login",
  REGISTER_PAGE: "/register",
  USER_TOKENS: "/tokens",
  KINDS_TOKENS: "/tokens/:id",
  GENERATE_TOKENS: "/tokens-generate",
  TOKEN_HISTORY: "/tokens-history",
  ACCEPT_TRANS: "/accepted",
  REJECT_TRANS: "/rejected",
  TOKEN_TRANS: "/transaction/:id",
  EVENT_SCANNERS: "/admin-panel/:eventId/scanners",
  EVENT_BUYABLE_TOKENS: "/admin-panel/:eventId/buyabletokens",
  EVENT_MORE_INFO: "/admin-panel/:eventId/eventmoreinformation",
  SCANNER_TRANSACTION_HISTORY: "/admin-panel/scanner-history/:scannerId",
  PAYU_GO_BACK: "/pay-payu",
};
