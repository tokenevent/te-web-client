export const primary = '#007bff';
export const RejectRed = '#f23a3a';
export const RejectLightRed = "#ff5151";
export const AcceptGreen = '#53aa3e';
export const AcceptLightGrey = "#649e55";
export const PendingOrange = '#ffa500';
export const PromotionPink = '#c466c3';
export const ErrorRed = RejectRed;
