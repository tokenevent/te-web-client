import styled from "styled-components";

const NoTokenToDisplay = styled.div`
    color: #999;
    font-size: 18px;
    text-align: center;
    margin-bottom: 130px;
`;
export default NoTokenToDisplay
