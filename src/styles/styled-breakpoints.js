import { css } from 'styled-components';

const sizes = {
    desktop: 992,
    tablet: 768,
    phone: 576,
}

export const mediaUp = Object.keys(sizes).reduce((acc, label) => {
    acc[label] = (...args) => css `
    @media (min-width: ${sizes[label]}px) {
        ${css(...args)}
    }
  `
    return acc
}, {})

export const mediaDown = Object.keys(sizes).reduce((acc, label) => {
    acc[label] = (...args) => css `
    @media (max-width: ${sizes[label] - 1}px) {
        ${css(...args)}
    }
  `
    return acc
}, {})

/* usage 
const Content = styled.div `
    height: 3em;
    width: 3em;
    background: papayawhip;

    Now we have our methods on media and can use them instead of raw queries 
    ${mediaUp.desktop`background: dodgerblue;`}
    ${mediaUp.tablet`background: mediumseagreen;`}
    ${mediaUp.phone`background: palevioletred;`}
`;
 */
