import styled from 'styled-components';

export const RowFull = styled.div`
    width: calc(100vw - 9px);
    position: relative;
    margin-left: -50vw;
    height: 100px;
    margin-top: 100px;
    left: 50%;
`;