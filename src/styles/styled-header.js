import styled from 'styled-components';

export const Header = styled.h1 `
    font-size: 28px;
    color: #222;
    text-align: center;

    &:after {
        content: '';
        display: block;
        border-bottom: 2px solid #222;
        width: 50px;
        margin: 25px auto;
    }
`;