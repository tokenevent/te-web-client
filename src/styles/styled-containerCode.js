import styled from 'styled-components';

export const ContainerCode = styled.h1 `
    font-size: 32px;
    color: #222;
    text-align: center;
`;