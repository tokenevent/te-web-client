import styled from 'styled-components';
import { mediaUp } from '../styles/styled-breakpoints';

export const TokensWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
`

export const SingleTokenWrapper = styled.div`
    box-sizing: border-box;
    border: 2px solid #007bff;
    color: #007bff;
    border-radius: 15px;
    width: calc(100% - 30px);
    margin: 15px;
    padding: 15px;
    display: flex;
    align-items: center;

    ${ mediaUp.tablet`
        width: calc(50% - 30px);
    `}

    img {
        max-width: 50px;
        max-height: 50px;
    }
`

export const TokenTitle = styled.div`
    width: 100%;
    margin-left: 20px;

    h2 {
        margin-top: 0;
        margin-bottom: 5px;

        span {
        color: #999;
        }
    }

  div {
    display: flex;
    justify-content: space-between;

    div:first-of-type {
        color: #999;
        font-size: 24px;
        line-height: 1;
        height: 25px;
        margin-top: 15px;
    }
}
`

export const AllTokensQuantity = styled.div`
    font-size: 50px;
    color: #007bff;

    span {
        font-size: 20px;
    }
`