import styled from 'styled-components';
import { mediaUp } from './styled-breakpoints';

export const ColorContainer = styled.div`
    background: rgb(0,82,204);
    background: linear-gradient(333deg, rgba(0,82,204,1) 0%, rgba(2,68,145,1) 100%);
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 15px;
`;

export const LogoWrapper = styled.div`
    padding: 15px 0;

    img {
        max-width: 100%;
    }
`;

export const FormWrapper = styled.div`
    width: 100%;
    background: #fff;
    position: relative;
    z-index: 1;
    max-width: 360px;
    margin: 0 auto 100px;
    padding: 45px;
    text-align: center;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);

    h1 {
        font-size: 24px;
        text-align: center;
        color: #333;
        margin-top: 0;
        text-transform: uppercase;
    }
`;

export const RegisterFormWrapper = styled(FormWrapper)`
    max-width: 700px;
`;

export const DoubleInputWrapper = styled.div`
    display: flex;
    flex-direction: column;

    ${mediaUp.tablet`
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    `}
`;

export const FormInputWrapper = styled.div`
    padding-bottom: 15px;

    label {
        display: none;
    }

    input {
        background: #f2f2f2;
        width: 100%;
        border: 1px solid #f2f2f2;
        margin: 0;
        padding: 15px;
        font-size: 14px;
        -webkit-appearance: none;
        border-radius: 0;
    }

    div {
        text-align: left;
        color: #f44336;
        font-size: 14px;
        margin-top: 3px;
        margin-left: 3px;
    }
`;

export const DoubleFormInputWrapper = styled(FormInputWrapper)`
    width: 100%;

    ${mediaUp.tablet`
        width: 49%;
    `}
`;

export const RedirectLinkWrapper = styled.div`
    color: #ccc;
    font-size: 12px;
    margin-top: 15px;

    a {
        color: #007bff;
        text-transform: uppercase;

        &:hover {
            color: #0561c3;
        }
    }
`;

export const SubmitButton = styled.button`
    border: 1px solid #007bff;
    background: #007bff;
    text-transform: uppercase;
    width: 100%;
    padding: 15px;
    color: #fff;
    font-size: 16px;
    transition: all 0.3s ease;
    cursor: pointer;
    -webkit-appearance: none;

    &:hover {
        background: transparent;
        color: #007bff;
    }
`;

export const SubmitErrorMessage = styled.div`
    margin-top: 15px;
    color: #f44336;
    font-size: 14px;
`;
