import styled, { css } from 'styled-components';

export const Icomoon = styled.i`
    &:before {
        font-family: 'icomoon' !important;
        speak: none;
        font-style: normal;
        font-weight: normal;
        font-variant: normal;
        text-transform: none;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    ${props => props.Facebook && css`
        &:before {
            content: '\\ea90';
        }
    `}

    ${props => props.Twitter && css`
        &:before {
            content: '\\ea92';
        }
    `}

    ${props => props.Instagram && css`
        &:before {
            content: '\\ea96';
        }
    `}
`;