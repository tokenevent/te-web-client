import styled from 'styled-components';

export const LoadingWrapper = styled.div `
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    background: rgba(255,255,255,.8);
    z-index: 99;
`;

export const LoadingInner = styled.div `
    display: inline-block;
    width: 64px;
    height: 64px;

    &:after {
        content: ' ';
        display: block;
        width: 46px;
        height: 46px;
        margin: 1px;
        border-radius: 50%;
        border: 5px solid #007bff;
        border-color: #007bff transparent #007bff transparent;
        animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
`;