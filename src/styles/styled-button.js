import styled, { css } from 'styled-components';
import {RejectRed} from './colors';

const baseButton = css`
    background: transparent;
    border-radius: 3px;
    border: 2px solid #007bff;
    color: #007bff;
    padding: 10px 25px;

    &:hover {
      background: #007bff;
      color: #fff;
    }

    ${props => props.primary && css`

      background: #007bff;
      color: #fff;

      &:hover {
        background: transparent;
        color: #007bff;
      }
    `}`;


export const Button = styled.button`
    ${baseButton}
`;

export const LinkButton = styled.a`
    ${baseButton}
    text-decoration: none;
`;

export const DeleteButton = styled(Button)`
  margin-left: 4px;
  background-color: ${RejectRed};
  color: white;
  transition: all .2s ease-in-out;
  border: 2px solid ${RejectRed};

  &:hover {
    background-color: white;
    color: ${RejectRed};
  }
`;