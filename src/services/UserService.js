import {api} from './../utils/enums';
import axios from 'axios';
import { authorizeRequest } from "../utils/request";

const userService = {
    loginUser: user => loginUser(user),
    createUser: user => createUser(user),
    getUser: () => getUser(),
    getBuyHistory: () => getBuyHistory()
};

const loginUser = user => {
    const url = api.USER_LOGIN;
    return axios.post(url, user);
};

const createUser = user => {
    const url = api.USER_REGISTER;
    return axios.post(url, user);
};

const getUser = () => {
    const url = api.CURRENT_USER;
    return authorizeRequest().get(url);
};

const getBuyHistory = () => {
    const url = api.BUY_HISTORY;
    return authorizeRequest().get(url);
}

export default userService;
