import {api} from './../utils/enums'
import {authorizeRequest} from './../utils/request';

const EventService = {
    getEvents: () => getEvents(),
    getEvent: id => getEvent(id),
    addEvent: event => addEvent(event),
    updateEvent: event => updateEvent(event),
    removeEvent: id => removeEvent(id),
    addBuyableTokens: (tokens, id) => addBuyableTokens(tokens, id),
    getManagerEvents: () => getManagerEvents()
};

const getEvents = () => {
    const url = api.EVENTS;
    return authorizeRequest().get(url);
};

const getEvent = id => {
    const url = api.EVENTS + "/" + id;
    return authorizeRequest().get(url);
};

const getManagerEvents = () => {
    const url = api.MANAGER_EVENTS;
    return authorizeRequest().get(url);
}

const addEvent = event => {
    const url = api.EVENTS_MANAGER;
    return authorizeRequest().post(url, event);
};

const updateEvent = event => {
    const url = api.EVENTS_MANAGER + "/" + event.id;
    return authorizeRequest().put(url, event);
};
const removeEvent = id => {
    const url = api.EVENTS_MANAGER + "/" + id;
    return authorizeRequest().delete(url);
};

const addBuyableTokens = (id, tokens) => {
    const url = api.EVENTS_MANAGER + "/" + id + "/buyable-tokens";
    return authorizeRequest().put(url, tokens);
};

export default EventService;
