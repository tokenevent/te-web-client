import {api} from './../utils/enums'
import {authorizeRequest} from './../utils/request';

const TokenService = {
    issueToken: (issue) => issueToken(issue),
    generateContainer: (tokens) => generateContainer(tokens),
    getWallet: () => getWallet(),
    getTokenHistory: () => getTokenHistory(),
    getTransaction: () => getTransaction(),
    postTransactionStatus: (payload) => postTransactionStatus(payload),
};

const issueToken = (issue) => {
    const url = api.ISSUE_TOKEN;
    return authorizeRequest().post(url, issue);
};

const generateContainer = (tokens) => {
    const url = api.GENERATE_CONTAINER;
    return authorizeRequest().post(url, tokens);
};

const getWallet = () => {
    const url = api.WALLET;
    return authorizeRequest().get(url);
};

const getTokenHistory = () => {
    const url = api.TRANSACTION_HISTORY;
    return authorizeRequest().get(url);
};

const getTransaction = () => {
    const url = api.TRANSACTION;
    return authorizeRequest().get(url);
}

const postTransactionStatus = (payload) => {
    const url = api.TRANSACTION;
    return authorizeRequest().post(url, payload);
}

export default TokenService;