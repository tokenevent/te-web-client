import { api } from './../utils/enums'
import { authorizeRequest } from './../utils/request';

const EventManagerService = {
  getScannersByEventId: eventId => getScannersByEventId(eventId),
  addScanner: (eventId, scannerData) => addScanner(eventId, scannerData),
  getTransactionsByScannerId: (scannerId, page) => getTransactionsByScannerId(scannerId, page),
  removeScannerByEventIdByScannerId: (eventId, scannerId) => removeScannerByEventIdByScannerId(eventId, scannerId),
  eventAddBuyableTokens: (eventId, token) => eventAddBuyableTokens(eventId, token),
  eventRemoveBuyableToken: (eventId, tokenName) => eventRemoveBuyableToken(eventId, tokenName),
  getTransactionDetailsByTransactionId: transactionId => getTransactionDetailsByTransactionId(transactionId),
  getBuyDetailsByTransactionId: transactionId => getBuyDetailsByTransactionId(transactionId)
};

const getScannersByEventId = eventId => {
  const url = api.EVENT_SCANNERS(eventId);
  return authorizeRequest().get(url);
};

const addScanner = (eventId, scannerData) => {
  const url = api.EVENT_SCANNERS(eventId);
  return authorizeRequest().post(url, scannerData);
};

const getTransactionsByScannerId = (scannerId, page) => {
  const url = api.SCANNER_TRANSACTION_HISTORY(scannerId, page);
  return authorizeRequest().get(url);
};

const removeScannerByEventIdByScannerId = (eventId, scannerId) => {
  const url = api.REMOVE_SCANNER_EVENT(eventId, scannerId);
  return authorizeRequest().delete(url);
};
const eventAddBuyableTokens = (eventId, token) => {
  const url = api.EVENT_ADD_BUYABLE_TOKEN(eventId);
  return authorizeRequest().put(url, [token]);
};

const eventRemoveBuyableToken = (eventId, tokenName) => {
  const url = api.EVENT_REMOVE_BUYABLE_TOKEN(eventId, tokenName);
  return authorizeRequest().delete(url, tokenName);
};

const getTransactionDetailsByTransactionId = transactionId => {
  const url = api.GET_SCANNER_TRANSACTION_DETAILS(transactionId);
  return authorizeRequest().get(url);
};

const getBuyDetailsByTransactionId = transactionId => {
  const url = api.GET_BUY_DETAILS(transactionId);
  return authorizeRequest().get(url);
};

export default EventManagerService;
